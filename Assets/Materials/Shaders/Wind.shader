// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE Shader/Wind"
{
	Properties
	{
		_WindStrength("Wind Strength", Range( 0 , 1)) = 0
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Color("Color", 2D) = "white" {}
		_Emission("Emission", 2D) = "white" {}
		[HideInInspector]_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Float4("Float 4", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float _WindStrength;
		uniform sampler2D _Color;
		uniform float4 _Color_ST;
		uniform sampler2D _Emission;
		uniform float4 _Emission_ST;
		uniform float _Float4;
		uniform float _Cutoff = 0.5;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 appendResult41 = (float4(ase_worldPos.x , ase_worldPos.z , 0.0 , 0.0));
			float lerpResult24 = lerp( ( tex2Dlod( _TextureSample0, float4( ( float4( ( float2( 0,0.35 ) * _Time.y ), 0.0 , 0.0 ) + ( appendResult41 / 2.0 ) ).xy, 0, 0.0) ).r * 0.5 ) , ( tex2Dlod( _TextureSample0, float4( ( float4( ( float2( 0,0.1 ) * _Time.y ), 0.0 , 0.0 ) + ( appendResult41 / 10.0 ) ).xy, 0, 0.0) ).r * 0.5 ) , 0.5);
			float lerpResult25 = lerp( 0.0 , lerpResult24 , pow( v.texcoord.xy.y , 3.0 ));
			float3 temp_cast_4 = (( lerpResult25 * _WindStrength )).xxx;
			v.vertex.xyz += temp_cast_4;
			v.normal = float3(0,0,1);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Color = i.uv_texcoord * _Color_ST.xy + _Color_ST.zw;
			float4 tex2DNode87 = tex2D( _Color, uv_Color );
			o.Albedo = tex2DNode87.rgb;
			float2 uv_Emission = i.uv_texcoord * _Emission_ST.xy + _Emission_ST.zw;
			o.Emission = ( tex2D( _Emission, uv_Emission ) * _Float4 ).rgb;
			o.Alpha = 1;
			clip( tex2DNode87.a - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
296;535;876;307;2911.479;609.3591;4.298988;True;False
Node;AmplifyShaderEditor.WorldPosInputsNode;16;-1847.524,315.6602;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleTimeNode;6;-1131.505,-116.786;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;23;-1137.596,486.9635;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-1122.169,445.8102;Float;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;5;-1135.475,-253.0721;Float;False;Constant;_Vector0;Vector 0;0;0;Create;True;0;0;False;0;0,0.35;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;13;-1119.08,786.1389;Float;False;Constant;_Float2;Float 2;0;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;19;-1139.766,350.6772;Float;False;Constant;_Vector1;Vector 1;0;0;Create;True;0;0;False;0;0,0.1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.DynamicAppendNode;41;-1542.162,323.9551;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-870.6868,402.0645;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;14;-905.9005,673.0085;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;3;-899.8099,69.25928;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-864.5961,-201.6849;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-716.8878,48.283;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-746.3736,630.437;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-435.0796,226.8313;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-442.1702,830.5806;Float;False;Constant;_Float3;Float 3;1;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;9;-578.9185,23.93553;Float;True;Property;_TextureSample0;Texture Sample 0;4;1;[HideInInspector];Create;True;0;0;False;0;None;1a77bc4c29edcf3458922640f607e714;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;18;-585.0089,627.6849;Float;True;Property;_TextureSample1;Texture Sample 1;4;1;[HideInInspector];Create;True;0;0;False;0;None;1a77bc4c29edcf3458922640f607e714;True;0;False;white;Auto;False;Instance;9;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;189.4182,530.8884;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-177.5938,726.6981;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-171.5032,122.9487;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;36;425.5063,524.3184;Float;False;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;24;196.1322,396.1946;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25;616.7351,440.0558;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;243.9477,704.7167;Float;False;Property;_WindStrength;Wind Strength;0;0;Create;True;0;0;False;0;0;0.2061876;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;90;670.678,285.2882;Float;False;Property;_Float4;Float 4;5;0;Create;True;0;0;False;0;0;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;563.7128,81.03241;Float;True;Property;_Emission;Emission;3;0;Create;True;0;0;False;0;None;eb2552dc6ac69b844b95a64e6e5a05c4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;976.5839,570.9966;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;38;978.431,363.9035;Float;False;Constant;_Vector2;Vector 2;4;1;[HideInInspector];Create;True;0;0;False;0;0,0,1;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;87;564.1353,-127.4146;Float;True;Property;_Color;Color;2;0;Create;True;0;0;False;0;None;eb2552dc6ac69b844b95a64e6e5a05c4;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;89;968.893,218.1258;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1324.855,115.5408;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ASE Shader/Wind;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;41;0;16;1
WireConnection;41;1;16;3
WireConnection;22;0;19;0
WireConnection;22;1;23;0
WireConnection;14;0;41;0
WireConnection;14;1;13;0
WireConnection;3;0;41;0
WireConnection;3;1;4;0
WireConnection;7;0;5;0
WireConnection;7;1;6;0
WireConnection;8;0;7;0
WireConnection;8;1;3;0
WireConnection;15;0;22;0
WireConnection;15;1;14;0
WireConnection;9;1;8;0
WireConnection;18;1;15;0
WireConnection;20;0;18;1
WireConnection;20;1;21;0
WireConnection;10;0;9;1
WireConnection;10;1;11;0
WireConnection;36;0;34;2
WireConnection;24;0;10;0
WireConnection;24;1;20;0
WireConnection;25;1;24;0
WireConnection;25;2;36;0
WireConnection;43;0;25;0
WireConnection;43;1;44;0
WireConnection;89;0;88;0
WireConnection;89;1;90;0
WireConnection;0;0;87;0
WireConnection;0;2;89;0
WireConnection;0;10;87;4
WireConnection;0;11;43;0
WireConnection;0;12;38;0
ASEEND*/
//CHKSM=128AA22E88BF99D8E81BFE99731A4A706D9AD00D