// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE Shader/Fx Smoke"
{
	Properties
	{
		_Color("Color", Color) = (0.754717,0.5233179,0.5233179,0)
		_Emission("Emission", Range( 0 , 1)) = 0
		_Cutoff( "Mask Clip Value", Float ) = 0.05
		[HideInInspector]_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_DissolveMask("Dissolve Mask", Range( 0 , 2)) = 0
		_VertexDisplacement("Vertex Displacement", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float _VertexDisplacement;
		uniform float4 _Color;
		uniform float _Emission;
		uniform float _DissolveMask;
		uniform float _Cutoff = 0.05;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 uv_TexCoord12 = v.texcoord.xy * float2( 2,2 );
			float4 tex2DNode5 = tex2Dlod( _TextureSample0, float4( uv_TexCoord12, 0, 0.0) );
			v.vertex.xyz += ( tex2DNode5 * _VertexDisplacement ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Albedo = _Color.rgb;
			float3 temp_cast_1 = (_Emission).xxx;
			o.Emission = temp_cast_1;
			o.Alpha = 1;
			float2 uv_TexCoord12 = i.uv_texcoord * float2( 2,2 );
			float4 tex2DNode5 = tex2D( _TextureSample0, uv_TexCoord12 );
			clip( ( tex2DNode5 * ( _SinTime.w + _DissolveMask ) ).r - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
308;448;1297;546;1539.884;138.4192;1.369221;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-1146.276,587.8076;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinTimeNode;17;-984.3115,-110.4671;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-1057.251,168.7523;Float;False;Property;_DissolveMask;Dissolve Mask;4;0;Create;True;0;0;False;0;0;1.1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;18;-673.3116,153.5654;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-771.6047,393.6419;Float;False;Property;_VertexDisplacement;Vertex Displacement;5;0;Create;True;0;0;False;0;0;0.1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;5;-816.1428,569.3921;Float;True;Property;_TextureSample0;Texture Sample 0;3;1;[HideInInspector];Create;True;0;0;False;0;None;df1cc5424e3c6bd4991c6b65702d74df;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;4;-552.2502,-206.021;Float;False;Property;_Emission;Emission;1;0;Create;True;0;0;False;0;0;0.231;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-382.4909,58.57381;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-327.9957,375.4137;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;3;-547.209,-126.7041;Float;False;Property;_Color;Color;0;0;Create;True;0;0;False;0;0.754717,0.5233179,0.5233179,0;1,1,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;25;-1352.7,775.6942;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ASE Shader/Fx Smoke;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.05;True;True;0;True;TransparentCutout;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;2;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;18;0;17;4
WireConnection;18;1;9;0
WireConnection;5;1;12;0
WireConnection;10;0;5;0
WireConnection;10;1;18;0
WireConnection;6;0;5;0
WireConnection;6;1;7;0
WireConnection;0;0;3;0
WireConnection;0;2;4;0
WireConnection;0;10;10;0
WireConnection;0;11;6;0
ASEEND*/
//CHKSM=FEE9E76EEDC2CC4C07DED60A43FAEB635001D30E