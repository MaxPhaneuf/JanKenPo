// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE Shader/Grass Cartoon"
{
	Properties
	{
		_GrassColorTop("Grass Color Top", Color) = (1,0.8509127,0.2279412,0)
		_GrassColorDown("Grass Color Down", Color) = (0,0.5147059,0.003549667,0)
		_GrassGradient("Grass Gradient", Range( 0 , 0.1)) = 0.02
		[HideInInspector]_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_DistanceGradient("Distance Gradient", Range( 0 , 1)) = 0.7411765
		_WindStrength("Wind Strength", Range( 0 , 1)) = 0.24
		[HideInInspector]_GrassColorNear("Grass Color Near", Color) = (0.09097358,0.5735294,0,0)
		[HideInInspector]_GrassColorFar("Grass Color Far", Color) = (0.6695949,0.9044118,0.4788062,0)
		[HideInInspector]_Gradient("Gradient", Range( 0 , 2)) = 0.05
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float4 screenPos;
		};

		uniform sampler2D _TextureSample0;
		uniform float _WindStrength;
		uniform float4 _GrassColorNear;
		uniform float4 _GrassColorFar;
		uniform float _Gradient;
		uniform float4 _GrassColorDown;
		uniform float4 _GrassColorTop;
		uniform float _GrassGradient;
		uniform float _DistanceGradient;


		float4 CalculateContrast( float contrastValue, float4 colorTarget )
		{
			float t = 0.5 * ( 1.0 - contrastValue );
			return mul( float4x4( contrastValue,0,0,t, 0,contrastValue,0,t, 0,0,contrastValue,t, 0,0,0,1 ), colorTarget );
		}

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float4 appendResult41 = (float4(ase_worldPos.x , ase_worldPos.z , 0.0 , 0.0));
			float lerpResult24 = lerp( ( tex2Dlod( _TextureSample0, float4( ( float4( ( float2( 0,0.35 ) * _Time.y ), 0.0 , 0.0 ) + ( appendResult41 / 2.0 ) ).xy, 0, 0.0) ).r * 0.5 ) , ( tex2Dlod( _TextureSample0, float4( ( float4( ( float2( 0,0.1 ) * _Time.y ), 0.0 , 0.0 ) + ( appendResult41 / 10.0 ) ).xy, 0, 0.0) ).r * 0.5 ) , 0.5);
			float lerpResult25 = lerp( 0.0 , lerpResult24 , pow( v.texcoord.xy.y , 3.0 ));
			float3 temp_cast_4 = (( lerpResult25 * _WindStrength )).xxx;
			v.vertex.xyz += temp_cast_4;
			v.normal = float3(0,0,1);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float3 desaturateInitialColor70 = float3( (ase_screenPos).xy ,  0.0 );
			float desaturateDot70 = dot( desaturateInitialColor70, float3( 0.299, 0.587, 0.114 ));
			float3 desaturateVar70 = lerp( desaturateInitialColor70, desaturateDot70.xxx, 1.0 );
			float4 lerpResult78 = lerp( _GrassColorNear , _GrassColorFar , CalculateContrast(_Gradient,float4( desaturateVar70 , 0.0 )).r);
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 temp_cast_3 = (( ase_vertex3Pos.y + _GrassGradient )).xxxx;
			float4 lerpResult79 = lerp( _GrassColorDown , _GrassColorTop , CalculateContrast(8.0,temp_cast_3).r);
			float4 temp_output_81_0 = ( lerpResult78 + lerpResult79 );
			float4 myVarName083 = temp_output_81_0;
			o.Albedo = myVarName083.rgb;
			float4 myVarName184 = ( temp_output_81_0 * _DistanceGradient );
			o.Emission = myVarName184.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
339;572;734;270;2057.106;147.8778;3.49693;True;False
Node;AmplifyShaderEditor.CommentaryNode;63;-1346.131,-1871.175;Float;False;1261.88;579.5038;Comment;8;78;77;73;72;71;70;66;65;Perspective Gradient;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;16;-1847.524,315.6602;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleTimeNode;23;-1137.596,486.9635;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;6;-1131.505,-116.786;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-1233.27,89.52286;Float;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;64;-1325.136,-1131.657;Float;False;1237.254;692.0206;Comment;7;79;76;75;74;69;68;67;Gradient Height Grass;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector2Node;5;-1135.475,-253.0721;Float;False;Constant;_Vector0;Vector 0;0;0;Create;True;0;0;False;0;0,0.35;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;19;-1139.766,350.6772;Float;False;Constant;_Vector1;Vector 1;0;0;Create;True;0;0;False;0;0,0.1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;13;-1119.08,786.1389;Float;False;Constant;_Float2;Float 2;0;0;Create;True;0;0;False;0;10;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;41;-1542.162,323.9551;Float;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;65;-1296.131,-1810.352;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;14;-905.9005,673.0085;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.PosVertexDataNode;67;-1197.48,-724.0542;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;68;-1275.136,-554.6359;Float;False;Property;_GrassGradient;Grass Gradient;2;0;Create;True;0;0;False;0;0.02;0.0203;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-870.6868,402.0645;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;66;-1078.599,-1807.983;Float;False;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-864.5961,-201.6849;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;3;-899.8099,69.25928;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-716.8878,48.283;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-1084.862,-1714.346;Float;False;Property;_Gradient;Gradient;8;1;[HideInInspector];Create;True;0;0;False;0;0.05;1.000977;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.DesaturateOpNode;70;-862.3246,-1821.175;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-746.3736,630.437;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;69;-935.6437,-705.9825;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;72;-701.8746,-1677.306;Float;False;Property;_GrassColorNear;Grass Color Near;6;1;[HideInInspector];Create;True;0;0;False;0;0.09097358,0.5735294,0,0;0.1008931,0.4056601,0.05166415,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;74;-1215.023,-899.1214;Float;False;Property;_GrassColorTop;Grass Color Top;0;0;Create;True;0;0;False;0;1,0.8509127,0.2279412,0;0.8679245,0.718165,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;75;-1216.262,-1081.657;Float;False;Property;_GrassColorDown;Grass Color Down;1;0;Create;True;0;0;False;0;0,0.5147059,0.003549667,0;0.4839681,0.6226414,0.2261478,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-435.0796,226.8313;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;9;-578.9185,23.93553;Float;True;Property;_TextureSample0;Texture Sample 0;3;1;[HideInInspector];Create;True;0;0;False;0;1a77bc4c29edcf3458922640f607e714;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleContrastOpNode;73;-660.8756,-1820.016;Float;False;2;1;COLOR;0,0,0,0;False;0;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleContrastOpNode;76;-658.7386,-712.5592;Float;True;2;1;COLOR;0.6226415,0.6226415,0.6226415,0;False;0;FLOAT;8;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;77;-700.9186,-1498.671;Float;False;Property;_GrassColorFar;Grass Color Far;7;1;[HideInInspector];Create;True;0;0;False;0;0.6695949,0.9044118,0.4788062,0;0.506478,0.6226414,0.3083836,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-442.1702,830.5806;Float;False;Constant;_Float3;Float 3;1;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-585.0089,627.6849;Float;True;Property;_TextureSample1;Texture Sample 1;3;1;[HideInInspector];Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;9;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;189.4182,530.8884;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;79;-352.8826,-905.6653;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;78;-349.2526,-1585.784;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-177.5938,726.6981;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-171.5032,122.9487;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;36;425.5063,524.3184;Float;False;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;24;196.1322,396.1946;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;80;66.37737,-1049.96;Float;False;Property;_DistanceGradient;Distance Gradient;4;0;Create;True;0;0;False;0;0.7411765;0.389;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;81;58.56035,-1588.211;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;44;243.9477,704.7167;Float;False;Property;_WindStrength;Wind Strength;5;0;Create;True;0;0;False;0;0.24;0.247;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;25;616.7351,440.0558;Float;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;382.5052,-1110.255;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;85;1052.419,26.15253;Float;False;83;0;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;83;537.2854,-1593.778;Float;False;myVarName0;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;976.5839,570.9966;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;38;978.431,363.9035;Float;False;Constant;_Vector2;Vector 2;4;1;[HideInInspector];Create;True;0;0;False;0;0,0,1;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;86;1006.27,243.9768;Float;False;84;0;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;84;540.4668,-1108.089;Float;False;myVarName1;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1324.855,115.5408;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ASE Shader/Grass Cartoon;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;41;0;16;1
WireConnection;41;1;16;3
WireConnection;14;0;41;0
WireConnection;14;1;13;0
WireConnection;22;0;19;0
WireConnection;22;1;23;0
WireConnection;66;0;65;0
WireConnection;7;0;5;0
WireConnection;7;1;6;0
WireConnection;3;0;41;0
WireConnection;3;1;4;0
WireConnection;8;0;7;0
WireConnection;8;1;3;0
WireConnection;70;0;66;0
WireConnection;15;0;22;0
WireConnection;15;1;14;0
WireConnection;69;0;67;2
WireConnection;69;1;68;0
WireConnection;9;1;8;0
WireConnection;73;1;70;0
WireConnection;73;0;71;0
WireConnection;76;1;69;0
WireConnection;18;1;15;0
WireConnection;79;0;75;0
WireConnection;79;1;74;0
WireConnection;79;2;76;0
WireConnection;78;0;72;0
WireConnection;78;1;77;0
WireConnection;78;2;73;0
WireConnection;20;0;18;1
WireConnection;20;1;21;0
WireConnection;10;0;9;1
WireConnection;10;1;11;0
WireConnection;36;0;34;2
WireConnection;24;0;10;0
WireConnection;24;1;20;0
WireConnection;81;0;78;0
WireConnection;81;1;79;0
WireConnection;25;1;24;0
WireConnection;25;2;36;0
WireConnection;82;0;81;0
WireConnection;82;1;80;0
WireConnection;83;0;81;0
WireConnection;43;0;25;0
WireConnection;43;1;44;0
WireConnection;84;0;82;0
WireConnection;0;0;85;0
WireConnection;0;2;86;0
WireConnection;0;11;43;0
WireConnection;0;12;38;0
ASEEND*/
//CHKSM=91C4239F8A98DB98A45B9F99B34CA79F60DF373E