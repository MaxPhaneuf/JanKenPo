// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE Shader/FireFx"
{
	Properties
	{
		_FireColorCenter("Fire Color Center", Color) = (0.9528302,0.7751923,0,0)
		_FireColorExterior("Fire Color Exterior", Color) = (0.9529412,0.5201435,0,0)
		_FireSpeed("Fire Speed", Range( 0 , 2)) = 1
		_FireDistortionScale("Fire Distortion Scale", Range( 0 , 0.1)) = 0.04
		_DistortionFactor("Distortion Factor", Range( 1 , 3)) = 2.3
		[HideInInspector]_TextureSample2("Texture Sample 2", 2D) = "white" {}
		[HideInInspector]_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector]_TextureSample1("Texture Sample 1", 2D) = "white" {}
		[HideInInspector]_TextureSample3("Texture Sample 3", 2D) = "white" {}
		[HideInInspector]_MaskClipValue("Mask Clip Value", Float) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _FireColorCenter;
		uniform float4 _FireColorExterior;
		uniform sampler2D _TextureSample1;
		uniform sampler2D _TextureSample0;
		uniform float _FireDistortionScale;
		uniform float _FireSpeed;
		uniform sampler2D _TextureSample2;
		uniform float _DistortionFactor;
		uniform sampler2D _TextureSample3;
		uniform float4 _TextureSample3_ST;
		uniform float _MaskClipValue;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 lerpResult58 = lerp( _FireColorCenter , _FireColorExterior , 0.5);
			float temp_output_31_0 = ( ( _FireDistortionScale * _FireSpeed ) * _Time.y );
			float2 uv_TexCoord27 = i.uv_texcoord * float2( 5,5 );
			float2 temp_output_32_0 = ( uv_TexCoord27 * _FireDistortionScale );
			float2 panner33 = ( temp_output_31_0 * float2( 0,-3 ) + temp_output_32_0);
			float2 panner34 = ( temp_output_31_0 * float2( 0,-2 ) + temp_output_32_0);
			float2 uv_TextureSample3 = i.uv_texcoord * _TextureSample3_ST.xy + _TextureSample3_ST.zw;
			float4 tex2DNode13 = tex2D( _TextureSample3, uv_TextureSample3 );
			float2 appendResult41 = (float2(i.uv_texcoord.x , ( pow( i.uv_texcoord.y , 2.0 ) + ( ( ( ( tex2D( _TextureSample0, panner33 ).r + tex2D( _TextureSample2, panner34 ).g ) * 0.33 ) * _DistortionFactor ) * ( pow( i.uv_texcoord.y , 0.9 ) * tex2DNode13.a ) ) )));
			float4 tex2DNode2 = tex2D( _TextureSample1, appendResult41 );
			float4 temp_output_52_0 = ( ( lerpResult58 * tex2DNode2.b ) + ( _FireColorCenter * pow( tex2DNode2.r , 0.0 ) ) + ( _FireColorExterior * tex2DNode2.g ) );
			o.Albedo = temp_output_52_0.rgb;
			o.Emission = temp_output_52_0.rgb;
			o.Alpha = 1;
			clip( ( ( 1.0 - tex2DNode2.b ) * tex2DNode13.a ) - _MaskClipValue );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
915;83;818;827;664.1348;1018.255;2.832645;True;False
Node;AmplifyShaderEditor.RangedFloatNode;28;-2194.738,24.3833;Float;False;Property;_FireDistortionScale;Fire Distortion Scale;3;0;Create;True;0;0;False;0;0.04;0.0405;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-2199.048,132.2805;Float;False;Property;_FireSpeed;Fire Speed;2;0;Create;True;0;0;False;0;1;0.93;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-2124.573,-134.4542;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;5,5;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;3;-1903.466,282.2894;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-1843.26,111.331;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-1616.385,109.5263;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-1616.295,-104.8736;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;34;-1405.096,140.3031;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-2;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;33;-1407.534,-108.3814;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-3;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;1;-1120.612,-81.69305;Float;True;Property;_TextureSample0;Texture Sample 0;6;1;[HideInInspector];Create;True;0;0;False;0;None;16d574e53541bba44a84052fa38778df;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;11;-1112.55,168.3088;Float;True;Property;_TextureSample2;Texture Sample 2;5;1;[HideInInspector];Create;True;0;0;False;0;None;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;20;-918.5824,392.823;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;12;-755.9649,67.30569;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-625.5872,181.0645;Float;False;Property;_DistortionFactor;Distortion Factor;4;0;Create;True;0;0;False;0;2.3;2.45;1;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;44;-643.4167,413.8094;Float;False;2;0;FLOAT;0;False;1;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;13;-984.2618,535.6937;Float;True;Property;_TextureSample3;Texture Sample 3;8;1;[HideInInspector];Create;True;0;0;False;0;None;52de98c3f51e6274a9a56baaf970e8ea;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-500.4744,65.90445;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0.33;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-320.4063,67.02652;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-419.6802,405.8182;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;25;-356.7056,-176.903;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;45;-59.87719,-218.4229;Float;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-107.4519,257.3997;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;43;159.5217,-152.3855;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;41;253.0918,147.009;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;49;397.3317,-63.25854;Float;False;Property;_FireColorExterior;Fire Color Exterior;1;0;Create;True;0;0;False;0;0.9529412,0.5201435,0,0;0.6072023,0.9150943,0.3410021,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;48;396.0984,-261.5934;Float;False;Property;_FireColorCenter;Fire Color Center;0;0;Create;True;0;0;False;0;0.9528302,0.7751923,0,0;0.4251798,0.9716981,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;472.6389,162.5988;Float;True;Property;_TextureSample1;Texture Sample 1;7;1;[HideInInspector];Create;True;0;0;False;0;None;52de98c3f51e6274a9a56baaf970e8ea;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;59;398.4395,-355.4836;Float;False;Constant;_ColormixBG;Color mix BG;7;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;58;824.931,-295.9187;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;60;844.0802,53.07183;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;46;876.7058,364.186;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;1028.846,-292.189;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;1030.602,-59.3631;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;1043.006,198.7994;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;1451.106,126.0444;Float;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;1075.649,442.2662;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;61;319.2149,441.071;Float;False;Constant;_MaskClipValue;Mask Clip Value;10;1;[HideInInspector];Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1920.994,179.4001;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ASE Shader/FireFx;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;False;0;False;TransparentCutout;;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;True;61;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;30;0;28;0
WireConnection;30;1;29;0
WireConnection;31;0;30;0
WireConnection;31;1;3;0
WireConnection;32;0;27;0
WireConnection;32;1;28;0
WireConnection;34;0;32;0
WireConnection;34;1;31;0
WireConnection;33;0;32;0
WireConnection;33;1;31;0
WireConnection;1;1;33;0
WireConnection;11;1;34;0
WireConnection;12;0;1;1
WireConnection;12;1;11;2
WireConnection;44;0;20;2
WireConnection;35;0;12;0
WireConnection;36;0;35;0
WireConnection;36;1;37;0
WireConnection;21;0;44;0
WireConnection;21;1;13;4
WireConnection;45;0;25;2
WireConnection;14;0;36;0
WireConnection;14;1;21;0
WireConnection;43;0;45;0
WireConnection;43;1;14;0
WireConnection;41;0;25;1
WireConnection;41;1;43;0
WireConnection;2;1;41;0
WireConnection;58;0;48;0
WireConnection;58;1;49;0
WireConnection;58;2;59;0
WireConnection;60;0;2;1
WireConnection;46;0;2;3
WireConnection;54;0;58;0
WireConnection;54;1;2;3
WireConnection;50;0;48;0
WireConnection;50;1;60;0
WireConnection;51;0;49;0
WireConnection;51;1;2;2
WireConnection;52;0;54;0
WireConnection;52;1;50;0
WireConnection;52;2;51;0
WireConnection;47;0;46;0
WireConnection;47;1;13;4
WireConnection;0;0;52;0
WireConnection;0;2;52;0
WireConnection;0;10;47;0
ASEEND*/
//CHKSM=A5A2A3751BC669B010B97CD63953E176D76A195F