// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE Shader/Water Cartoon"
{
	Properties
	{
		_GradientColorDown("Gradient Color Down", Color) = (0,0.2849226,0.3773585,0)
		_GradientColorUp("Gradient Color Up", Color) = (0.3632075,1,0.9229687,0)
		[HideInInspector]_TextureFishDistortion("Texture Fish Distortion", 2D) = "white" {}
		[HideInInspector]_TextureDistortion("Texture Distortion", 2D) = "bump" {}
		_GradientPosition("Gradient Position", Range( -1 , 2)) = 0
		_WaveSpeed("Wave Speed", Range( 0 , 0.5)) = 0
		_FishSpeed("Fish Speed", Range( 0 , 0.5)) = 0
		_WaveDistortion("Wave Distortion", Range( 0 , 0.04)) = 0
		[HideInInspector]_TextureWaves("Texture Waves", 2D) = "white" {}
		[HideInInspector]_TextureCaustics2("Texture Caustics 2", 2D) = "white" {}
		[HideInInspector]_TextureCaustics("Texture Caustics", 2D) = "white" {}
		_FoamCaustics("Foam/Caustics", Range( 0 , 1)) = 0.5
		_Emission("Emission", Range( 0 , 1)) = 0
		[HideInInspector]_TextureSample3("Texture Sample 3", 2D) = "white" {}
		[HideInInspector]_TextureSample2("Texture Sample 2", 2D) = "white" {}
		[HideInInspector]_TextureSample1("Texture Sample 1", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float4 screenPos;
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform sampler2D _TextureSample1;
		uniform float _FishSpeed;
		uniform sampler2D _CameraDepthTexture;
		uniform sampler2D _TextureFishDistortion;
		uniform float4 _TextureFishDistortion_ST;
		uniform sampler2D _TextureSample2;
		uniform sampler2D _TextureSample3;
		uniform float4 _GradientColorDown;
		uniform float4 _GradientColorUp;
		uniform float _GradientPosition;
		uniform float _WaveSpeed;
		uniform float _WaveDistortion;
		uniform sampler2D _TextureDistortion;
		uniform float4 _TextureDistortion_ST;
		uniform sampler2D _TextureCaustics2;
		uniform float _FoamCaustics;
		uniform sampler2D _TextureWaves;
		uniform sampler2D _TextureCaustics;
		uniform float _Emission;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float mulTime255 = _Time.y * _FishSpeed;
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float eyeDepth282 = LinearEyeDepth(UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture,UNITY_PROJ_COORD(ase_screenPos))));
			float temp_output_285_0 = abs( ( eyeDepth282 - ase_screenPos.w ) );
			float2 temp_cast_0 = (temp_output_285_0).xx;
			float2 panner256 = ( mulTime255 * float2( 2,1 ) + temp_cast_0);
			float2 uv_TexCoord257 = i.uv_texcoord + panner256;
			float2 uv_TextureFishDistortion = i.uv_texcoord * _TextureFishDistortion_ST.xy + _TextureFishDistortion_ST.zw;
			float4 tex2DNode271 = tex2D( _TextureFishDistortion, uv_TextureFishDistortion );
			float2 temp_cast_3 = (temp_output_285_0).xx;
			float2 panner264 = ( mulTime255 * float2( 1,-1 ) + temp_cast_3);
			float2 uv_TexCoord262 = i.uv_texcoord + panner264;
			float2 temp_cast_6 = (temp_output_285_0).xx;
			float2 panner265 = ( mulTime255 * float2( -1,1 ) + temp_cast_6);
			float2 uv_TexCoord266 = i.uv_texcoord + panner265;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float clampResult39 = clamp( ( ase_vertex3Pos.y + _GradientPosition ) , 0.0 , 0.5 );
			float4 lerpResult34 = lerp( _GradientColorDown , _GradientColorUp , clampResult39);
			float mulTime199 = _Time.y * _WaveSpeed;
			float2 panner202 = ( mulTime199 * float2( -1,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord203 = i.uv_texcoord + panner202;
			float2 uv_TextureDistortion = i.uv_texcoord * _TextureDistortion_ST.xy + _TextureDistortion_ST.zw;
			float3 tex2DNode215 = UnpackScaleNormal( tex2D( _TextureDistortion, uv_TextureDistortion ) ,_WaveDistortion );
			float2 uv_TexCoord204 = i.uv_texcoord + panner202;
			float4 lerpResult195 = lerp( tex2D( _TextureWaves, ( float3( uv_TexCoord203 ,  0.0 ) + tex2DNode215 ).xy ) , tex2D( _TextureCaustics2, ( float3( uv_TexCoord204 ,  0.0 ) + tex2DNode215 ).xy ) , _FoamCaustics);
			float mulTime216 = _Time.y * _WaveSpeed;
			float2 panner222 = ( mulTime216 * float2( 1,0.5 ) + float2( 0,0 ));
			float2 uv_TexCoord218 = i.uv_texcoord * float2( 2,2 ) + panner222;
			float2 uv_TexCoord221 = i.uv_texcoord + panner222;
			float4 lerpResult212 = lerp( tex2D( _TextureWaves, ( float3( uv_TexCoord218 ,  0.0 ) + tex2DNode215 ).xy ) , tex2D( _TextureCaustics, ( float3( uv_TexCoord221 ,  0.0 ) + tex2DNode215 ).xy ) , _FoamCaustics);
			float4 temp_output_154_0 = ( lerpResult34 + ( lerpResult195 + lerpResult212 ) );
			o.Albedo = ( ( tex2D( _TextureSample1, ( float4( uv_TexCoord257, 0.0 , 0.0 ) + tex2DNode271 ).rg ) * tex2D( _TextureSample2, ( float4( uv_TexCoord262, 0.0 , 0.0 ) + tex2DNode271 ).rg ) * tex2D( _TextureSample3, ( float4( uv_TexCoord266, 0.0 , 0.0 ) + tex2DNode271 ).rg ) ) * temp_output_154_0 ).rgb;
			o.Emission = ( temp_output_154_0 * _Emission ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
621;206;818;827;-1286.207;675.5361;1.861788;True;False
Node;AmplifyShaderEditor.ScreenPosInputsNode;281;-547.8763,975.1033;Float;False;1;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;213;-854.4866,-774.3002;Float;False;Property;_WaveSpeed;Wave Speed;11;0;Create;True;0;0;False;0;0;0.04;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenDepthNode;282;-297.9282,977.6243;Float;False;0;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;199;-215.417,-1016.961;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;216;-282.5688,-416.4134;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;284;-52.23238,1009.806;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;261;-122.1918,787.5883;Float;False;Property;_FishSpeed;Fish Speed;12;0;Create;True;0;0;False;0;0;0.04;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;214;-502.609,-751.5878;Float;False;Property;_WaveDistortion;Wave Distortion;17;0;Create;True;0;0;False;0;0;0.04;0;0.04;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;222;-46.38663,-457.7289;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,0.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;202;-31.67504,-1060.436;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;255;185.3872,786.2063;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;285;98.88454,1022.106;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;218;214.0759,-635.9252;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;203;228.209,-1211.565;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;215;-175.8867,-796.9958;Float;True;Property;_TextureDistortion;Texture Distortion;6;1;[HideInInspector];Create;True;0;0;False;0;None;0bebe40e9ebbecc48b8e9cfea982da7e;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;204;218.5882,-982.6194;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;221;187.2006,-375.0537;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;278;421.8556,-51.21241;Float;False;1154.67;573.3753;Gradient water;7;34;39;36;37;35;33;32;;1,1,1,1;0;0
Node;AmplifyShaderEditor.PannerNode;264;395.0122,1016.65;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;256;390.1115,670.4586;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;2,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;206;544.8171,-1198.39;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;219;475.5134,-563.8013;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;35;778.8303,255.8546;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;265;390.1477,1169.967;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;207;524.2922,-943.7329;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;220;477.4886,-368.1837;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;36;682.2111,404.3015;Float;False;Property;_GradientPosition;Gradient Position;8;0;Create;True;0;0;False;0;0;0.15;-1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;271;698.3918,807.9373;Float;True;Property;_TextureFishDistortion;Texture Fish Distortion;5;1;[HideInInspector];Create;True;0;0;False;0;None;1b27ecac227c38741af291b0e8fc5ff2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;257;611.3386,672.3347;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;193;766.8948,-1170.32;Float;True;Property;_TextureSample10;Texture Sample 10;19;1;[HideInInspector];Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;209;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;210;776.8355,-380.1347;Float;True;Property;_TextureCaustics;Texture Caustics;21;1;[HideInInspector];Create;True;0;0;False;0;None;66304d7d1b2b67545b1592b7c5a44e51;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;209;643.75,-598.6788;Float;True;Property;_TextureWaves;Texture Waves;19;1;[HideInInspector];Create;True;0;0;False;0;None;37a85118c4dc1c54898420b41e302f36;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;262;604.582,1018.527;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;211;782.6096,-720.2723;Float;False;Property;_FoamCaustics;Foam/Caustics;22;0;Create;True;0;0;False;0;0.5;0.9058235;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;194;770.1807,-959.5836;Float;True;Property;_TextureCaustics2;Texture Caustics 2;20;1;[HideInInspector];Create;True;0;0;False;0;None;9c886051bd8435648ba4f9a472ac7c46;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;266;597.386,1171.843;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;37;986.4945,278.633;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;272;1093.546,717.933;Float;False;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;273;1118.392,891.7793;Float;False;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;274;1117.425,1181.476;Float;False;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;195;1193.824,-979.7601;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;33;565.9648,177.2726;Float;False;Property;_GradientColorUp;Gradient Color Up;2;0;Create;True;0;0;False;0;0.3632075,1,0.9229687,0;0.6078432,1,1,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;39;1111.842,273.9415;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;212;1179.691,-404.1192;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;32;564.725,-5.263021;Float;False;Property;_GradientColorDown;Gradient Color Down;1;0;Create;True;0;0;False;0;0,0.2849226,0.3773585,0;0,0.3764703,0.3764703,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;267;1330.074,1142.052;Float;True;Property;_TextureSample3;Texture Sample 3;24;1;[HideInInspector];Create;True;0;0;False;0;None;9655749d5979e0c47ab0ccedc3f04fbd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;263;1326.189,872.3883;Float;True;Property;_TextureSample2;Texture Sample 2;25;1;[HideInInspector];Create;True;0;0;False;0;None;a58f639ab7ed2454bb5c520986acc870;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;260;1320.712,609.9044;Float;True;Property;_TextureSample1;Texture Sample 1;26;1;[HideInInspector];Create;True;0;0;False;0;None;1a004a27df854084b8db235191d6bccd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;224;1661.09,-394.3202;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;34;1279.52,174.2248;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;226;1846.736,231.2923;Float;False;Property;_Emission;Emission;23;0;Create;True;0;0;False;0;0;0.715;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;269;1867.502,829.4683;Float;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;154;1900.186,-21.49576;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PannerNode;158;1922.174,3401.044;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;159;1524.938,3790.211;Float;False;Property;_Float1;Float 1;15;0;Create;True;0;0;False;0;0;0.04;0;0.04;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;176;2841.674,3546.856;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;177;2813.027,4468.149;Float;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;161;1732.623,5087.341;Float;False;Property;_Float2;Float 2;9;0;Create;True;0;0;False;0;0;0.4;-1;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;186;2020.049,5430.616;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.05,-0.05;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;165;2049.907,4995.472;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;174;2434.435,4615.522;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleTimeNode;157;1649.971,4142.644;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;185;1744.63,5334.508;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;258;2318.754,-160.6105;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleTimeNode;156;1728.294,3540.737;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;167;2393.613,3493.495;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;168;2384.552,3968.122;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ClampOpNode;170;2216.353,4979.581;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;181;2019.793,5221.797;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.1,0.1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;178;3337.787,4023.046;Float;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;192;2466.17,5448.766;Float;True;Property;_TextureSample9;Texture Sample 9;3;1;[Normal];Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Instance;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;0.5;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;162;1809.742,4923.294;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;190;2701.126,4959.05;Float;False;Property;_Float4;Float 4;18;0;Create;True;0;0;False;0;0;0.3640352;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;191;2697.904,4759.508;Float;True;Property;_TextureSample8;Texture Sample 8;14;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BlendNormalsNode;188;2867.4,5418.974;Float;True;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;184;1731.077,5575.463;Float;False;Property;_Float3;Float 3;10;0;Create;True;0;0;False;0;119.0621;154;0;360;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;182;2406.08,5210.467;Float;True;Property;_TextureSample7;Texture Sample 7;4;1;[Normal];Create;True;0;0;False;0;None;None;True;0;True;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;171;1784.137,4511.376;Float;False;Constant;_Color1;Color 1;0;0;Create;True;0;0;False;0;0,0.4357396,0.4716981,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;179;3215.621,3613.483;Float;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;3419.779,4621.467;Float;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;270;402.269,856.8812;Float;False;Property;_FishDistortion;Fish Distortion;16;0;Create;True;0;0;False;0;0;0.1;0;0.2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;180;2237.722,5480.146;Float;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;160;1878.106,4015.029;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,0.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;164;2099.504,4006.743;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;163;2146.481,3399.612;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;225;2161.505,86.15256;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;169;1785.377,4693.912;Float;False;Constant;_Color0;Color 0;0;0;Create;True;0;0;False;0;0.514151,1,0.9397247,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RadiansOpNode;183;2028.03,5580.376;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;166;1813.98,3742.619;Float;True;Property;_TextureSample4;Texture Sample 4;7;0;Create;True;0;0;False;0;None;0bebe40e9ebbecc48b8e9cfea982da7e;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;173;2515.25,3470.412;Float;True;Property;_TextureSample6;Texture Sample 6;3;0;Create;True;0;0;False;0;None;cd460ee4ac5c1e746b7a734cc7cc64dd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;187;3168.034,4798.84;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;155;1257.032,3755.224;Float;False;Property;_Float0;Float 0;13;0;Create;True;0;0;False;0;0;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;175;2381.22,4287.597;Float;True;Spherical;World;False;Top Texture 0;_TopTexture0;white;0;None;Mid Texture 1;_MidTexture1;white;-1;None;Bot Texture 1;_BotTexture1;white;-1;None;Triplanar Sampler;False;9;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;8;FLOAT;1;False;3;FLOAT;0.9;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;172;2522.072,3654.262;Float;True;Property;_TextureSample5;Texture Sample 5;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2573.157,-46.64463;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ASE Shader/Water Cartoon;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;282;0;281;0
WireConnection;199;0;213;0
WireConnection;216;0;213;0
WireConnection;284;0;282;0
WireConnection;284;1;281;4
WireConnection;222;1;216;0
WireConnection;202;1;199;0
WireConnection;255;0;261;0
WireConnection;285;0;284;0
WireConnection;218;1;222;0
WireConnection;203;1;202;0
WireConnection;215;5;214;0
WireConnection;204;1;202;0
WireConnection;221;1;222;0
WireConnection;264;0;285;0
WireConnection;264;1;255;0
WireConnection;256;0;285;0
WireConnection;256;1;255;0
WireConnection;206;0;203;0
WireConnection;206;1;215;0
WireConnection;219;0;218;0
WireConnection;219;1;215;0
WireConnection;265;0;285;0
WireConnection;265;1;255;0
WireConnection;207;0;204;0
WireConnection;207;1;215;0
WireConnection;220;0;221;0
WireConnection;220;1;215;0
WireConnection;257;1;256;0
WireConnection;193;1;206;0
WireConnection;210;1;220;0
WireConnection;209;1;219;0
WireConnection;262;1;264;0
WireConnection;194;1;207;0
WireConnection;266;1;265;0
WireConnection;37;0;35;2
WireConnection;37;1;36;0
WireConnection;272;0;257;0
WireConnection;272;1;271;0
WireConnection;273;0;262;0
WireConnection;273;1;271;0
WireConnection;274;0;266;0
WireConnection;274;1;271;0
WireConnection;195;0;193;0
WireConnection;195;1;194;0
WireConnection;195;2;211;0
WireConnection;39;0;37;0
WireConnection;212;0;209;0
WireConnection;212;1;210;0
WireConnection;212;2;211;0
WireConnection;267;1;274;0
WireConnection;263;1;273;0
WireConnection;260;1;272;0
WireConnection;224;0;195;0
WireConnection;224;1;212;0
WireConnection;34;0;32;0
WireConnection;34;1;33;0
WireConnection;34;2;39;0
WireConnection;269;0;260;0
WireConnection;269;1;263;0
WireConnection;269;2;267;0
WireConnection;154;0;34;0
WireConnection;154;1;224;0
WireConnection;158;1;156;0
WireConnection;176;0;173;0
WireConnection;176;1;172;0
WireConnection;177;0;175;0
WireConnection;177;1;174;0
WireConnection;186;0;185;0
WireConnection;165;0;162;2
WireConnection;165;1;161;0
WireConnection;174;0;171;0
WireConnection;174;1;169;0
WireConnection;174;2;170;0
WireConnection;157;0;155;0
WireConnection;258;0;269;0
WireConnection;258;1;154;0
WireConnection;156;0;155;0
WireConnection;167;0;163;0
WireConnection;167;1;166;0
WireConnection;168;0;164;0
WireConnection;168;1;166;0
WireConnection;170;0;165;0
WireConnection;181;0;185;0
WireConnection;178;0;177;0
WireConnection;178;1;176;0
WireConnection;192;1;180;0
WireConnection;188;0;182;0
WireConnection;188;1;192;0
WireConnection;182;1;181;0
WireConnection;179;0;177;0
WireConnection;179;1;176;0
WireConnection;189;0;177;0
WireConnection;189;1;187;0
WireConnection;180;0;186;0
WireConnection;180;2;183;0
WireConnection;160;1;157;0
WireConnection;164;1;160;0
WireConnection;163;1;158;0
WireConnection;225;0;154;0
WireConnection;225;1;226;0
WireConnection;183;0;184;0
WireConnection;166;5;159;0
WireConnection;173;1;167;0
WireConnection;187;0;191;0
WireConnection;187;1;190;0
WireConnection;172;1;168;0
WireConnection;0;0;258;0
WireConnection;0;2;225;0
ASEEND*/
//CHKSM=45B857A4E1BCF46A8AE10C6455812A78FF534E69