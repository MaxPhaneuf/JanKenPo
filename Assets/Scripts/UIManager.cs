﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [Tooltip("Player slots available at selection")]
    public GameObject[] playerSlots;
    [Tooltip("Player color available at selection (Temporary)")]
    public Material[] playerColorMats;
    [Tooltip("Text to start the game")]
    public GameObject startText;
    public GameObject characterSelect, victoryPanel, CountdownPanel;
    [HideInInspector] public int activatedPlayers;

    public GameObject MenuScreen;
    public GameObject OptionScreen;
    public GameObject LoadingScreen;

    public Slider loadingSlider;

    public static UIManager instance;

    private void Start()
    {
        if (!instance)
            instance = this;
    }

    /// <summary>
    /// Activate slot for a player
    /// </summary>
    /// <param name="i"> Index of slot to activate </param>
    public void ActivateSlot(int i)
    {
        playerSlots[i].GetComponent<Image>().material = playerColorMats[i];
        activatedPlayers++;
        if (activatedPlayers >= GameManager.instance.minNbPlayers)
            startText.SetActive(true);
    }

    /// <summary>
    /// Deactivate slot for a player
    /// </summary>
    /// <param name="i"> Index of slot to deactivate </param>
    public void DeactivateSlot(int i)
    {
        playerSlots[i].GetComponent<Image>().material = null;
        activatedPlayers--;
        if (activatedPlayers < GameManager.instance.minNbPlayers)
            startText.SetActive(false);
    }

    public void DeactivateMenu()
    {
        if(MenuScreen.activeSelf)
            MenuScreen.SetActive(false);
    }

    public void ActivateMenu()
    {
        if(!MenuScreen.activeSelf)
            MenuScreen.SetActive(true);
    }

    public void DeactivateOption()
    {
        if (OptionScreen.activeSelf)
            OptionScreen.SetActive(false);
    }

    public void ActivateOption()
    {
        if (!OptionScreen.activeSelf)
            OptionScreen.SetActive(true);
    }

    public void ActivateLoadingScreen()
    {
        if (!LoadingScreen.activeSelf)
            LoadingScreen.SetActive(true);
    }

    public void DeactivateLoadingScreen()
    {
        if (LoadingScreen.activeSelf)
            LoadingScreen.SetActive(false);
    }

    public void ChangeLoadingSlider(float progress)
    {
        loadingSlider.value = progress;            
    }
}
