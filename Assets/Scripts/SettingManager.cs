﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;

public class SettingManager : MonoBehaviour {

    public Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Dropdown textureQualityDropdown;
    public Slider musicSlider;
    public Button applyButton;

    public AudioSource musicSource;

    Resolution[] resolutions;

    [System.Serializable]
    public class GameSettings
    {
        public bool fullscreen;
        public int textureQuality;
        public int resolutionIndex;
        public float musicVolume;

        public GameSettings(GameSettings settings)
        {
            fullscreen = settings.fullscreen;
            textureQuality = settings.textureQuality;
            resolutionIndex = settings.resolutionIndex;
            musicVolume = settings.musicVolume;
        }

    }
    public GameSettings gameSettings;

    void OnEnable()
    {
        //gameSettings = new GameSettings();

        resolutions = Screen.resolutions;

        fullscreenToggle.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        resolutionDropdown.onValueChanged.AddListener(delegate { OnResolutionChange(); });
        textureQualityDropdown.onValueChanged.AddListener(delegate { OnTextureQualityChange(); });
        musicSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChange(); });
        applyButton.onClick.AddListener(delegate { OnApplyButtonClick(); });

        foreach(Resolution resolution in resolutions)
        {
            resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }

        if(File.Exists(Application.persistentDataPath + "/gamesettings.json"))
            LoadSettings();
    }

    public void OnFullscreenToggle()
    {
        gameSettings.fullscreen = Screen.fullScreen = fullscreenToggle.isOn;

    }

    // Change the resolution of the screen and save it in gameSettings' script
    public void OnResolutionChange()
    {
        Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, Screen.fullScreen);
        gameSettings.resolutionIndex = resolutionDropdown.value;
    }

    // Change the quality of the texture and save it in gameSettings' script
    public void OnTextureQualityChange()
    {
        QualitySettings.masterTextureLimit = gameSettings.textureQuality = textureQualityDropdown.value;
    }

    // Change the volume of the music and save it in gameSettings' script
    public void OnMusicVolumeChange()
    {
        musicSource.volume = gameSettings.musicVolume = musicSlider.value;
    }

    public void OnApplyButtonClick()
    {
        SaveSettings();
    }
    
    // Translate the gameSettings in BinaryFormat and save the informations in a file
    void SaveSettings()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream stream = new FileStream(Application.persistentDataPath + "/gameSettings.sav", FileMode.Create);
        GameSettings data = new GameSettings(gameSettings);

        bf.Serialize(stream, data);
        stream.Close();
    }

    // Load the saved settings from the binary file to the current gameSettings
    void LoadSettings()
    {
        if(File.Exists(Application.persistentDataPath + "/gameSettings.sav"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(Application.persistentDataPath + "/gameSettings.sav", FileMode.Open);

            GameSettings data = bf.Deserialize(stream) as GameSettings;
            gameSettings = data;

            stream.Close();
        }

        musicSlider.value = gameSettings.musicVolume;
        fullscreenToggle.isOn = gameSettings.fullscreen;
        resolutionDropdown.value = gameSettings.resolutionIndex;
        textureQualityDropdown.value = gameSettings.textureQuality;
    }
}
