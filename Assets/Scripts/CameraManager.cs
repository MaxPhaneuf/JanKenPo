﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    #region Private attributes
    CameraEffect currentEffect;
    Quaternion endRotation;
    Vector3 endPosition, startPosition;
    float shakeTimer;
    Queue<CameraEffect> queue = new Queue<CameraEffect>();
    #endregion

    /// <summary>
    /// Internal class to manage camera effects
    /// </summary>
    [System.Serializable]
    public class CameraEffect
    {

        public string EffectName = "New Effect";
        public enum EffectType { None, MoveTo, Shake }
        [Tooltip("With a rotation?")]
        public bool withRotation;
        public EffectType effectType;
        public RotationEffect rotationEffect;
        public MoveEffect moveEffect;
        public ShakeEffect shakeEffect;
        [HideInInspector] public bool atTargetRot, atTargetPos, shakeDone, active;

        [System.Serializable]
        public struct RotationEffect
        {
            public enum Axis { X, Y, Z }
            [Tooltip("Axis to rotate Camera on")]
            public Axis axis;
            [Tooltip("Main camera rotation angle")]
            public float Angle;
            [Tooltip("Speed for this effect")]
            public float Speed;
        }

        [System.Serializable]
        public struct MoveEffect
        {
            public enum SpecialTarget { None, Winner }
            [Tooltip("Axis that will go to the target")]
            public bool x, y, z;
            [Tooltip("Move target for this effect")]
            public Vector3 Target;
            [Tooltip("Gives Target a special value taken at runtime\nWinner : Uses y axis value above to zoom on winner, x and z axis will go to winner's transform")]
            public SpecialTarget specialTarget;
            [Tooltip("Speed for this effect")]
            public float Speed;
        }

        [System.Serializable]
        public struct ShakeEffect
        {
            public float Duration;
            public float Magnitude;
        }

    }

    public List<CameraEffect> effects;
    [HideInInspector] public bool inRotEffect, inMoveEffect, inShakeEffect;

    public static CameraManager instance;

    void Start()
    {
        if (!instance)
            instance = this;
    }

    private void Update()
    {
        if (inRotEffect || inMoveEffect || inShakeEffect)
            PlayCamEffect(currentEffect);
        if (effects.Count > 0 && currentEffect != null && IsCurrentEffectDone())
        {
            ResetCurrent();
            PlayNextEffect();
        }
    }

    /// <summary>
    /// Play effect i in effect list.
    /// </summary>
    /// <param name="i"></param>
    public void PlayCamEffect(CameraEffect effect)
    {
        if (effect.withRotation)
            RotationEffect(effect.rotationEffect.Speed);
        if (effect.effectType == CameraEffect.EffectType.MoveTo)
            MoveEffect(effect.moveEffect.Speed);
        else if (effect.effectType == CameraEffect.EffectType.Shake)
            ShakeEffect(effect.shakeEffect.Duration, effect.shakeEffect.Magnitude);
    }

    /// <summary>
    /// Is current effect finish playing.
    /// </summary>
    /// <returns></returns>
    bool IsCurrentEffectDone()
    {
        return currentEffect.active
            && currentEffect.atTargetPos
            && currentEffect.atTargetRot
            && currentEffect.shakeDone;
    }

    void ResetCurrent()
    {
        inMoveEffect = false;
        inRotEffect = false;
        inShakeEffect = false;
        currentEffect.active = false;
        currentEffect.atTargetPos = false;
        currentEffect.atTargetRot = false;
        currentEffect.shakeDone = false;
    }

    /// <summary>
    /// Play the next effect in the queue.
    /// </summary>
    void PlayNextEffect()
    {
        if (queue.Count > 0)
        {
            CameraEffect effect = queue.Dequeue();
            ActivateEffect(effect);
        }
    }

    /// <summary>
    /// Activate effect with index.
    /// </summary>
    public void ActivateEffect(int i)
    {
        ActivationManager(effects[i]);
    }

    /// <summary>
    /// Activate effect with name.
    /// </summary>
    public void ActivateEffect(string effectName)
    {
        ActivationManager(FindByName(effectName));
    }

    /// <summary>
    /// Find an effect by name.
    /// </summary>
    CameraEffect FindByName(string effectName)
    {
        for (int i = 0; i < effects.Count; i++)
        {
            if (effects[i].EffectName == effectName)
                return effects[i];
        }
        return null;
    }

    /// <summary>
    /// Activate or add effect to queue.
    /// </summary>
    /// <param name="effect"></param>
    void ActivationManager(CameraEffect effect)
    {
        if (IsPlayingEffect())
            queue.Enqueue(effect);
        else
            ActivateEffect(effect);
    }

    public bool IsPlayingEffect()
    {
        return inMoveEffect || inRotEffect || inShakeEffect;
    }

    /// <summary>
    /// Activate a camera effect.
    /// </summary>
    /// <param name="effect"></param>
    void ActivateEffect(CameraEffect effect)
    {
        Rotation(effect);
        Movement(effect);
        Shake(effect);
        effect.active = true;
        currentEffect = effect;
    }

    /// <summary>
    /// Activate or complete rotation effect.
    /// </summary>
    /// <param name="effect"></param>
    void Rotation(CameraEffect effect)
    {
        if (effect.withRotation)
            ActivateRotationEffect(effect.rotationEffect);
        else
            effect.atTargetRot = true;
    }

    /// <summary>
    /// Activate or complete movement effect.
    /// </summary>
    /// <param name="effect"></param>
    void Movement(CameraEffect effect)
    {
        if (effect.effectType == CameraEffect.EffectType.MoveTo)
            MoveTargetManager(effect);
        else
            effect.atTargetPos = true;
    }

    /// <summary>
    /// Change movement target depending on special condition.
    /// </summary>
    /// <param name="effect"></param>
    void MoveTargetManager(CameraEffect effect)
    {
        SpecialMoveEffect(effect);
        ActivateMoveEffect(effect.moveEffect);
    }

    void ChangeMoveEffectTarget(CameraEffect effect, Vector3 pos)
    {
        effect.moveEffect.Target = new Vector3(pos.x, pos.y, pos.z);
    }

    /// <summary>
    /// Activate or complete shake effect.
    /// </summary>
    /// <param name="effect"></param>
    void Shake(CameraEffect effect)
    {
        if (effect.effectType == CameraEffect.EffectType.Shake)
            ActivateShakeEffect();
        else
            effect.shakeDone = true;
    }

    /// <summary>
    /// Activate rotation effect.
    /// </summary>
    void ActivateRotationEffect(CameraEffect.RotationEffect rotationEffect)
    {
        endRotation = GetRotation(rotationEffect.Angle, transform.rotation, rotationEffect.axis);
        inRotEffect = true;
    }

    /// <summary>
    /// Activate move effect.
    /// </summary>
    void ActivateMoveEffect(CameraEffect.MoveEffect moveEffect)
    {
        endPosition = GetPosition(moveEffect.x, moveEffect.y, moveEffect.z, moveEffect.Target, transform.position);
        inMoveEffect = true;
    }

    /// <summary>
    /// Activate special move effect.
    /// </summary>
    void SpecialMoveEffect(CameraEffect effect)
    {
        if (effect.moveEffect.specialTarget == CameraEffect.MoveEffect.SpecialTarget.Winner && GameManager.instance.winner)
            TargetWinnerWithZoom(effect);
    }
    
    void TargetWinnerWithZoom(CameraEffect effect)
    {
        Transform winner = GameManager.instance.winner.transform;
        Vector3 pos = new Vector3(winner.position.x, winner.position.y + effect.moveEffect.Target.y, winner.position.z);
        ChangeMoveEffectTarget(effect, pos);
    }

    /// <summary>
    /// Activate effect shake effect.
    /// </summary>
    void ActivateShakeEffect()
    {
        startPosition = transform.position;
        inShakeEffect = true;
    }

    /// <summary>
    /// Play a frame of the rotation effect.
    /// </summary>
    /// <param name="speed"></param>
    void RotationEffect(float speed)
    {
        if (Vector3.Distance(transform.rotation.eulerAngles, endRotation.eulerAngles) > 0)
            transform.rotation = Quaternion.RotateTowards(transform.rotation, endRotation, speed);
        else
            EndRotation();
            
    }

    /// <summary>
    /// End rotation effect.
    /// </summary>
    void EndRotation()
    {
        inRotEffect = false;
        currentEffect.atTargetRot = true;
    }

    /// <summary>
    /// Play a frame of the move effect.
    /// </summary>
    /// <param name="speed"></param>
    void MoveEffect(float speed)
    {
        if (Vector3.Distance(transform.position, endPosition) > 0)
            transform.position = Vector3.MoveTowards(transform.position, endPosition, speed);
        else
            EndMove();
    }

    /// <summary>
    /// End movement effect.
    /// </summary>
    void EndMove()
    {
        inMoveEffect = false;
        currentEffect.atTargetPos = true;
    }

    /// <summary>
    /// Play a frame of the shake effect.
    /// </summary>
    /// <param name="duration"></param>
    /// <param name="magnitude"></param>
    void ShakeEffect(float duration, float magnitude)
    {
        if (shakeTimer < duration)
            RandomShake(magnitude);
        else
            EndShake();
    }

    /// <summary>
    /// Apply random position on x and y axis this frame.
    /// </summary>
    /// <param name="magnitude"></param>
    void RandomShake(float magnitude)
    {
        float x = Random.Range(-1f, 1f) * magnitude;
        float y = Random.Range(-1f, 1f) * magnitude;
        transform.position = new Vector3(startPosition.x + x, startPosition.y + y, startPosition.z);
        shakeTimer += Time.deltaTime;
    }

    /// <summary>
    /// End shake effect.
    /// </summary>
    void EndShake()
    {
        transform.position = startPosition;
        shakeTimer = 0;
        inShakeEffect = false;
        currentEffect.shakeDone = true;
    }

    /// <summary>
    /// Get new rotation with axis value (1 = X, 2 = Y, 3 = Z).
    /// </summary>
    Quaternion GetRotation(float angle, Quaternion rot, CameraEffect.RotationEffect.Axis axis)
    {
        switch (axis)
        {
            case CameraEffect.RotationEffect.Axis.X:
                return Quaternion.Euler(rot.eulerAngles.x + angle, rot.eulerAngles.y, rot.eulerAngles.z);
            case CameraEffect.RotationEffect.Axis.Y:
                return Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y + angle, rot.eulerAngles.z);
            case CameraEffect.RotationEffect.Axis.Z:
                return Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y, rot.eulerAngles.z + angle);
            default:
                return rot;
        }
    }

    /// <summary>
    /// Position depending on chosen axis and target.
    /// </summary>
    Vector3 GetPosition(bool x, bool y, bool z, Vector3 target, Vector3 pos)
    {
        float _x = pos.x, _y = pos.y, _z = pos.z;
        if (x) _x = target.x;
        if (y) _y = target.y;
        if (z) _z = target.z;
        return new Vector3(_x, _y, _z);
    }

}
