﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public Button playButton;
    public Button optionButton;
    public Button quitButton;
    public Button backButton;

    public int MainScene;
    public int MenuScene;
	// Use this for initialization
	void OnEnable()
    {
        playButton.onClick.AddListener(delegate { OnPlayButtonClick(); });
        optionButton.onClick.AddListener(delegate { OnOptionButtonClick(); });
        quitButton.onClick.AddListener(delegate { OnQuitButtonClick(); });
        backButton.onClick.AddListener(delegate { OnBackButtonClick(); });
    }

    // On Play button click this show the loading screen and load the next scene
    public void OnPlayButtonClick()
    {
        UIManager.instance.DeactivateMenu();
        UIManager.instance.ActivateLoadingScreen();
        StartCoroutine(LoadSceneAsynch(MainScene));
    }

    // Load the scene and show on the loading screen the progress of the loading with a slider
    IEnumerator LoadSceneAsynch(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        operation.allowSceneActivation = false;

        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            UIManager.instance.ChangeLoadingSlider(progress);
            if (progress == 1f)
                operation.allowSceneActivation = true;

            yield return null;
        }

    }

    public void OnOptionButtonClick()
    {
        UIManager.instance.DeactivateMenu();
        UIManager.instance.ActivateOption();
    }

    public void OnQuitButtonClick()
    {
        Application.Quit();
    }

    public void OnBackButtonClick()
    {
        UIManager.instance.DeactivateOption();
        UIManager.instance.ActivateMenu();
    }
}
