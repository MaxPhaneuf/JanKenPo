﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    const string SPLIT = "|";
    public string[] controllerNames;
    public List<Controller> registeredPlayers = new List<Controller>();
    public Dictionary<int, Controller> controllers = new Dictionary<int, Controller>();
    public Material[] stance; //For now only material changes.
    public static GameManager instance;

    void Awake()
    {
        if (!instance)
            instance = this;
        controllerNames = Input.GetJoystickNames();
        RegisterPlayers();
        AssignControllerNames();
    }
        
    void RegisterPlayers()
    {
        GameObject[] list = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject o in list)
        {
            registeredPlayers.Add(o.GetComponent<Controller>());
        }
    }

    void AssignControllerNames()
    {
        for (int i = 0; i < registeredPlayers.Count; i++)
        {
            if (!controllers.ContainsKey(registeredPlayers[i].controllerID))
            {
                if (i < controllerNames.Length && controllerNames[i] != "")
                {
                    registeredPlayers[i].ControllerName = SPLIT + (i + 1).ToString();
                    registeredPlayers[i].controllerID = i + 1;
                }
                controllers.Add(registeredPlayers[i].controllerID, registeredPlayers[i]);
            }
            else
            {
                registeredPlayers[i].ControllerName = Controller.NOT_PLAYING;
                registeredPlayers[i].gameObject.SetActive(false);
            }
        }
    }
}
