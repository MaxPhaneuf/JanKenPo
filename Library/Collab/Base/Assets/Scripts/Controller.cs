﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{

    const string HORIZONTAL_INPUT = "Horizontal";
    const string VERTICAL_INPUT = "Vertical";
    const string FIRE1_INPUT = "Fire1";
    const string FIRE2_INPUT = "Fire2";
    const string FIRE3_INPUT = "Fire3";
    const string JUMP_INPUT = "Jump";
    public const string NOT_PLAYING = "Not Playing";
    bool moving, acted, slashed;
    Vector3 lastPosition;

    float moveOffSet = .1f, minDist = .1f;
    int speed = 1;

    Inputs.Direction lastDirection = Inputs.Direction.Up, beforeSlash = Inputs.Direction.Up;
    public int controllerID = -1;
    public Inputs inputs = new Inputs();
    public LayerMask playerMask, wallMask;
    [HideInInspector] public string ControllerName;
    public enum Action { Jan, Ken, Po }
    public Action currentAction;
    public GameObject otherPlayer;

    [System.Serializable]
    public class Inputs
    {
        public float Horizontal, Vertical;
        public bool Button1, Button2, Button3, jump;
        public enum Direction { Up, Right, Down, Left }
        public Direction direction;
        public void SetDirection()
        {
            if (Horizontal > 0)
                direction = Direction.Right;
            else if (Horizontal < 0)
                direction = Direction.Left;
            else if (Vertical > 0)
                direction = Direction.Up;
            else if (Vertical < 0)
                direction = Direction.Down;
        }
        public void ResetInputs()
        {
            Button1 = false;
            Button2 = false;
            Button3 = false;
        }
    }

    void Update()
    {
        if (ControllerName != NOT_PLAYING)
        {
            InputManager();
            if (lastDirection != inputs.direction)
                RotateToCurrentDirection();
            if (moving)
                Move();
            else
                IsMoving();
        }
    }

    void InputManager()
    {
        if (!moving)
            ManageMovement();
        if (PlayerSeen())
            ManageActions();
        if (WallSeen())
        {
            Debug.Log("True");
            acted = false;
            inputs.ResetInputs();
        }
    }

    bool IsMoving()
    {
        moving = IsButtonPressed();
        if (moving)
            lastPosition = transform.position;
        return moving;
    }
    void ManageMovement()
    {
        inputs.Horizontal = Input.GetAxisRaw(HORIZONTAL_INPUT + ControllerName);
        inputs.Vertical = Input.GetAxisRaw(VERTICAL_INPUT + ControllerName);
        inputs.SetDirection();
        acted = ButtonInputs();
        ChangeCurrentAction();
        ChangeCurrentState(); // For now only material changes.
    }

    void ManageActions()
    {
        if (acted && CompareAction())
            EnemyHit();
        inputs.ResetInputs();
    }

    void EnemyHit()
    {
        lastPosition = transform.position;
        moving = true;
        acted = false;
    }

    bool CompareAction()
    {
        Action enemyAction = otherPlayer.GetComponent<Controller>().currentAction;
        switch (currentAction)
        {
            case Action.Jan:
                return enemyAction == Action.Ken;
            case Action.Ken:
                return enemyAction == Action.Po;
            case Action.Po:
                return enemyAction == Action.Jan;
        }
        return false;
    }

    bool PlayerSeen()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1, playerMask))
        {
            if (hit.collider.GetComponentInParent<Controller>())
            {
                otherPlayer = hit.collider.transform.parent.gameObject;
                return true;
            }
        }
        return false;
    }

    bool WallSeen()
    {
        return Physics.Raycast(transform.position, transform.forward, 1, wallMask);
    }

    bool ButtonInputs()
    {
        inputs.Button1 = Input.GetButtonDown(FIRE1_INPUT + ControllerName);
        inputs.Button2 = Input.GetButtonDown(FIRE2_INPUT + ControllerName);
        inputs.Button3 = Input.GetButtonDown(FIRE3_INPUT + ControllerName);
        return IsButtonPressed();
    }

    bool IsButtonPressed()
    {
        return inputs.Button1 || inputs.Button2 || inputs.Button3;
    }

    void ChangeCurrentAction()
    {
        if (inputs.Button1)
            currentAction = Action.Jan;
        else if (inputs.Button2)
            currentAction = Action.Ken;
        else if (inputs.Button3)
            currentAction = Action.Po;
    }

    void ChangeCurrentState()
    {
        switch (currentAction)
        {
            case Action.Jan:
                GetComponentInChildren<Renderer>().material = GameManager.instance.stance[0];
                break;
            case Action.Ken:
                GetComponentInChildren<Renderer>().material = GameManager.instance.stance[1];
                break;
            case Action.Po:
                GetComponentInChildren<Renderer>().material = GameManager.instance.stance[2];
                break;
        }
    }

    void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, (lastPosition + transform.forward) * speed, moveOffSet);
        if (Vector3.Distance(transform.position, lastPosition + transform.forward) <= minDist)
        {
            transform.position = lastPosition + transform.forward;
            moving = false;
        }
    }

    void RotateToCurrentDirection()
    {
        float newAngle = GetAngle(lastDirection, inputs.direction);
        transform.Rotate(new Vector3(0, newAngle, 0));
        lastDirection = inputs.direction;
    }

    float GetAngle(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = GetAngleDelta(lastDir, newDir);
        if (Mathf.Abs(diff) == 2)
            return 180;
        else if (diff == 1)
            return 90;
        else if (diff == -1)
            return -90;
        return 0;
    }

    int GetAngleDelta(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = 0;
        if (newDir == Inputs.Direction.Left && lastDir == Inputs.Direction.Up)
            diff = -1;
        else if (lastDir == Inputs.Direction.Left && newDir == Inputs.Direction.Up)
            diff = 1;
        else
            diff = (int)newDir - (int)lastDir;
        return diff;
    }

}
