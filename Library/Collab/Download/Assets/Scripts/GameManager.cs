﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages most aspect of the game
/// </summary>
public class GameManager : MonoBehaviour
{
    
    #region Constants
    const string SPLIT = "|";
    const string KEYBOARD_START_INPUT = "Start";
    const string KEYBOARD_BUTTON1_INPUT = "Fire1";
    const string KEYBOARD_BUTTON2_INPUT = "Fire2";
    #endregion
     
    #region Private attributes
    int keyboardIndex;
    bool buttonPressed, gameStarted;
    string[] joystickNames;
    #endregion

    [Tooltip("Layer for obstacles")]
    public LayerMask wallMask, stairsMask, fogMask;
    [Tooltip("Minimum number of players mandatory to start a game")]
    public int minNbPlayers = 2;
    [Tooltip("Total lives for each players(Zero means defeat")]
    public int numberOfLives = 3;
    public float timeBeforeNewGame = 3;
    public int secondsForCoundown = 3;
    [Tooltip("List of instantiate player objects.")]
    public List<Controller> players = new List<Controller>();
    
    [Tooltip("Color materials to represent changing of stance (Temporary)")]
    public Material[] colorMats;
    [HideInInspector] public int activePlayer;
    [HideInInspector] public GameObject winner;
    
    // Maps of all registered controllers.
    public Dictionary<int, Controller> controllers = new Dictionary<int, Controller>(); 

    public static GameManager instance;
        
    void Awake()
    {
        if (!instance)
            instance = this;
        // Get all joysticks only at the start of the game, later connected joysticks are ignored.
        joystickNames = Input.GetJoystickNames(); 
    }

    private void Update()
    {
        if (UIManager.instance.characterSelect && UIManager.instance.characterSelect.activeInHierarchy)
            ListenToInputs();
        if (gameStarted && activePlayer == 1 && winner)
            StartCoroutine(Victory(timeBeforeNewGame));
    }

    #region Static Methods
    /// <summary>
    /// Find objet by name in transform childs
    /// </summary>
    /// <param name="childName"></param>
    /// <returns></returns>
    public static GameObject FindInChilds(Transform tr, string childName)
    {
        foreach (Transform child in tr)
        {
            if (child.name == childName)
                return child.gameObject;
        }
        return null;
    }

    /// <summary>
    /// Looks for a wall in front of this player.
    /// </summary>
    /// <returns> Is a wall in front of this player? </returns>
    public static bool LookWithLayer(Vector3 from, Vector3 direction, LayerMask mask, float distance)
    {
        return Physics.Raycast(from, direction, distance, mask);
    }
    #endregion

    public bool LookForFog(Vector3 from, Vector3 direction, float distance)
    {
        return LookWithLayer(from, direction, fogMask, distance);
    }

    public bool LookForWall(Vector3 from, Vector3 direction, float distance)
    {
        return LookWithLayer(from, direction, wallMask, distance);
    }

    public bool LookForStairs(Vector3 from, Vector3 direction, float distance)
    {
        return LookWithLayer(from, direction, stairsMask, distance);
    }
        
    /// <summary>
    /// Listen to all inputs.
    /// </summary>

    void ListenToInputs()
    {
        JoystickInputs();
        if (!buttonPressed)
            KeyboardInputs();
        buttonPressed = false;
    }

    /// <summary>
    /// Happens when only one player is left.
    /// </summary>
    IEnumerator Victory(float sec)
    {
        CameraManager.instance.ActivateEffect("Zoom Winner");
        UIManager.instance.victoryPanel.SetActive(true);
        gameStarted = false;
        yield return new WaitForSecondsRealtime(sec);
        ResetForNewGame();
        while (CameraManager.instance.IsPlayingEffect())
        {
            yield return new WaitForEndOfFrame();
        }
        UIManager.instance.characterSelect.SetActive(true);
    }

    void ResetForNewGame()
    {
        UIManager.instance.victoryPanel.SetActive(false);
        ResetPlayers();
        CameraManager.instance.ActivateEffect("BackToSelect");
    }

    void ResetPlayers()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].ControllerName != "" || players[i].keyboard)
                players[i].ResetPlayer();
        }
    }
    
    /// <summary>
    /// Listen to inputs from joysticks.
    /// </summary>
    void JoystickInputs()
    {
        for (int i = 0; i < joystickNames.Length; i++)
        {
            if (joystickNames[i] != "")
            {
                if (IsButtonDown(Controller.BUTTON1_INPUT, i) && !controllers.ContainsKey(i + 1))
                    RegisterController(i);
                if (IsButtonDown(Controller.BUTTON2_INPUT, i) && controllers.ContainsKey(i + 1))
                    UnregisterController(i);
                if (IsButtonDown(Controller.START_INPUT, i) && controllers.Count >= minNbPlayers)
                    StartCoroutine(StartCountdown(secondsForCoundown)); 

            }
        }
    }

    /// <summary>
    /// Listen to inputs from keyboard.
    /// </summary>
    void KeyboardInputs()
    {
        if (Input.GetButtonDown(KEYBOARD_BUTTON1_INPUT) && !controllers.ContainsKey(-1))
            RegisterController(-1);
        if (Input.GetButtonDown(KEYBOARD_BUTTON2_INPUT) && controllers.ContainsKey(-1))
            UnregisterController(-1);
        if (Input.GetButtonDown(KEYBOARD_START_INPUT) && controllers.Count >= minNbPlayers)
            StartGame();
    }

    /// <summary>
    /// Check controller for input.
    /// </summary>
    /// <param name="inputName"> Input to look for </param>
    /// <param name="i"> Controller index </param>
    /// <returns> Is that button down? </returns>
    bool IsButtonDown(string inputName, int i)
    {
        buttonPressed = Input.GetButtonDown(inputName + SPLIT + (i + 1).ToString());
        return buttonPressed;
    }

    /// <summary>
    /// Assign controller to player
    /// </summary>
    /// <param name="i"> Controller and player index base </param>
    void RegisterController(int i)
    {
        int index = 0;
        if (i != -1)
            index = RegisterJoystick(i);
        else
            index = RegisterKeyboard();
        UIManager.instance.ActivateSlot(index);
    }

    /// <summary>
    /// Register a joystick with a player.
    /// </summary>
    /// <param name="i"> Controller index </param>
    /// <returns> Index of current player </returns>
    int RegisterJoystick(int i)
    {
        int index = i;
        if (players[i].keyboard && i + 1 < players.Count)
            index++;
        players[index].ControllerName = SPLIT + (i + 1).ToString(); // Registered with +1 for readable purpose (Not a fan of controller 0)
        players[index].playerIndex = index;
        controllers.Add(i + 1, players[index]);
        return index;
    }

    /// <summary>
    /// Register the keyboard with a player.
    /// </summary>
    /// <param name="i"> Controller index </param>
    /// <returns> Index of current player </returns>
    int RegisterKeyboard()
    {
        keyboardIndex = controllers.Count;
        players[keyboardIndex].keyboard = true;
        controllers.Add(-1, players[keyboardIndex]);
        return keyboardIndex;
    }

    /// <summary>
    /// Unassign controller from player.
    /// </summary>
    /// <param name="i"> Controller and player index base </param>
    void UnregisterController(int i)
    {
        int index = 0;
        if (i != -1)
            index = UnregisterJoystick(i + 1);
        else
            index = UnregisterKeyboard();
        UIManager.instance.DeactivateSlot(index);
    }

    /// <summary>
    /// Unassign joystick from player.
    /// </summary>
    /// <param name="i"> Joystick index </param>
    /// <returns> Index of current player </returns>
    int UnregisterJoystick(int i)
    {
        int index = controllers[i].playerIndex;
        controllers[i].ControllerName = "";
        controllers.Remove(i);
        return index;
    }

    /// <summary>
    /// Unassign keyboard from player.
    /// </summary>
    /// <returns> Index of current player </returns>
    int UnregisterKeyboard()
    {
        controllers[-1].keyboard = false;
        controllers.Remove(-1);
        return keyboardIndex;
    }

    public IEnumerator StartCountdown(int sec)
    {
        UIManager.instance.characterSelect.SetActive(false);
        CameraManager.instance.ActivateEffect("New Game");
        while (CameraManager.instance.IsPlayingEffect())
        {
            yield return new WaitForEndOfFrame();
        }
        Text cd = UIManager.instance.CountdownPanel.GetComponentInChildren<Text>();
        cd.text = sec.ToString();
        
        UIManager.instance.CountdownPanel.SetActive(true);
        for (int i = 0; i < sec; i++)
        {
            cd.text = (sec - i).ToString();
            yield return new WaitForSecondsRealtime(1);
        }
        UIManager.instance.CountdownPanel.SetActive(false);
        StartGame();
    }

    /// <summary>
    /// Start the game for all players. Players without controller are not activated.
    /// </summary>
    void StartGame()
    {
        ActivatePlayersWithController();
        activePlayer = controllers.Count;
        gameStarted = true;
    }

    void ActivatePlayersWithController()
    {
        for (int i = 0; i < players.Count; i++)
        {
            if ((players[i].ControllerName != "" || players[i].keyboard) && players[i].ControllerName != Controller.NOT_PLAYING)
                players[i].gameObject.SetActive(true);
            else
                players[i].ControllerName = Controller.NOT_PLAYING;
        }
    }

    /// <summary>
    /// Returns first found active player.
    /// </summary>
    /// <returns></returns>
    public GameObject FindActivePlayer()
    {
        foreach (Controller player in players)
        {
            if (player.gameObject.activeInHierarchy)
                return player.gameObject;
        }
        return null;
    }

    public Controller FindClosestPlayer(GameObject from)
    {
        float distance = float.MaxValue;
        float temp;
        Controller closest = null;
        foreach (Controller player in players)
        {
            if (player.gameObject.activeInHierarchy && player.name != from.name)
            {
                temp = Vector3.Distance(from.transform.position, player.transform.position);
                if (temp < distance)
                {
                    distance = temp;
                    closest = player;
                }
            }
        }
        return closest;
    }

    /// <summary>
    /// Assign match winner depending on sender.
    /// </summary>
    /// <param name="sender"></param>
    public void AssignWinner(GameObject sender)
    {
        if (!sender)
            winner = FindActivePlayer();
        else
            winner = sender;
    }

    /// <summary>
    /// When a player dies.
    /// </summary>
    /// <param name="sender"></param>
    public void DeathOfPlayer(GameObject sender)
    {
        activePlayer--;
        CameraManager.instance.ActivateEffect("ShakeDead");
        if (activePlayer == 1)
            AssignWinner(sender);
    }

}