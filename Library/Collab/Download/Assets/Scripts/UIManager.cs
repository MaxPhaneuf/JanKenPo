﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    [Tooltip("Player slots available at selection")]
    public GameObject[] playerSlots;
    [Tooltip("Player color available at selection (Temporary)")]
    public Material[] playerColorMats;
    [Tooltip("Text to start the game")]
    public GameObject startText;
    public GameObject characterSelect, victoryPanel, CountdownPanel;
    [HideInInspector] public int activatedPlayers;

    public static UIManager instance;

    private void Start()
    {
        if (!instance)
            instance = this;
    }

    /// <summary>
    /// Activate slot for a player
    /// </summary>
    /// <param name="i"> Index of slot to activate </param>
    public void ActivateSlot(int i)
    {
        playerSlots[i].GetComponent<Image>().material = playerColorMats[i];
        activatedPlayers++;
        if (activatedPlayers >= GameManager.instance.minNbPlayers)
            startText.SetActive(true);
    }

    /// <summary>
    /// Deactivate slot for a player
    /// </summary>
    /// <param name="i"> Index of slot to deactivate </param>
    public void DeactivateSlot(int i)
    {
        playerSlots[i].GetComponent<Image>().material = null;
        activatedPlayers--;
        if (activatedPlayers < GameManager.instance.minNbPlayers)
            startText.SetActive(false);
    }
}
