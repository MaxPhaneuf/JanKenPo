﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Script for everything related to a player's controls in game world.
/// </summary>
public class Controller : MonoBehaviour
{
    #region Constants
    public const string HORIZONTAL_INPUT = "Horizontal";
    public const string VERTICAL_INPUT = "Vertical";
    public const string BUTTON1_INPUT = "Fire1";
    public const string BUTTON2_INPUT = "Fire2";
    public const string BUTTON3_INPUT = "Fire3";
    public const string MOVE_INPUT = "Jump";
    public const string START_INPUT = "Start";
    public const string NOT_PLAYING = "Not Playing";
    public enum Action { Jan, Ken, Po }
    #endregion

    #region Parameters
    // To be assign by GameManager instance
    [HideInInspector] public string ControllerName;
    Rigidbody Rb { get { return GetComponent<Rigidbody>(); } }
    Controller EnemySeen { get { return otherPlayer.GetComponent<Controller>(); } }
    #endregion

    #region Private attributes
    public bool moving, hit, recoil, stairs, grounded;
    Vector3 lastPosition;
    public Vector3 endPosition;
    float minDist = .1f, hitTimer = 0;
    Inputs.Direction lastDirection = Inputs.Direction.Up;
    [HideInInspector] public int lives;
    GameObject model;
    GameObject otherPlayer;
    [HideInInspector] public Inputs inputs = new Inputs();
    [HideInInspector] public Vector3 startPosition;
    #endregion

    [Tooltip("Layer for players")]
    public LayerMask playerMask;
    [Tooltip("This player's current action")]
    public Action currentAction;
    [Tooltip("This player's move speed (transform.forward * moveSpeed)")]
    public float moveSpeed = 7;
    [Tooltip("Time taken to recover from hit")]
    public float hitTime = .5f;
    [HideInInspector] public bool keyboard;
    [HideInInspector] public int playerIndex;

    /// <summary>
    /// Internal Class to manage and show in inspector this player's inputs.
    /// </summary>
    [System.Serializable]
    public class Inputs
    {
        public float Horizontal, Vertical;
        public bool Button1, Button2, Button3, MoveButton;
        public enum Direction { Up, Right, Down, Left }
        [HideInInspector] public Direction direction;

        public void SetDirection(string ControllerName)
        {
            Horizontal = Input.GetAxisRaw(HORIZONTAL_INPUT + ControllerName);
            Vertical = Input.GetAxisRaw(VERTICAL_INPUT + ControllerName);
            if (Horizontal > 0)
                direction = Direction.Right;
            else if (Horizontal < 0)
                direction = Direction.Left;
            else if (Vertical > 0)
                direction = Direction.Up;
            else if (Vertical < 0)
                direction = Direction.Down;
        }

        public void SetButtons(string ControllerName)
        {
            Button1 = Input.GetButtonDown(BUTTON1_INPUT + ControllerName);
            Button2 = Input.GetButtonDown(BUTTON2_INPUT + ControllerName);
            Button3 = Input.GetButtonDown(BUTTON3_INPUT + ControllerName);
            MoveButton = Input.GetButtonDown(MOVE_INPUT + ControllerName);
        }

        public void ResetButtons()
        {
            Button1 = false;
            Button2 = false;
            Button3 = false;
            MoveButton = false;
        }
    }

    private void Start()
    {
        lives = GameManager.instance.numberOfLives;
        model = GameManager.FindInChilds(transform, "Model");
        startPosition = transform.position;
    }

    void Update()
    {
        if (ControllerName != NOT_PLAYING && !UIManager.instance.characterSelect.activeInHierarchy)
        {
            if (!hit)
                Move();
            else
                HitEffect();
            if (IsAtPosition(transform.position.x, endPosition.x)
                && IsAtPosition(transform.position.z, endPosition.z))
                EndMove(endPosition);
            if (GameManager.instance.LookForFog(transform.position, -transform.up, 1)) // if is outside the map
                Dead(null);
            grounded = GameManager.instance.LookForWall(transform.position, -transform.up, .5f);
        }
    }

    /// <summary>
    /// Manages different stages of movement
    /// </summary>
    void Move()
    {
        if (!moving && !stairs)
            BeforeMove();
        else
            DuringMove();
    }

    /// <summary>
    /// Functions before and starting movement.
    /// </summary>
    void BeforeMove()
    {
        if (grounded)
            InputManager();
        if (moving)
            StartMove(transform.forward);
    }

    /// <summary>
    /// Function to start movement.
    /// </summary>
    void StartMove(Vector3 direction)
    {
        lastPosition = transform.position;
        if (GameManager.instance.LookForStairs(transform.position, direction, 1))
            direction = AdjustMoveForStairs(direction);
        else
            endPosition = lastPosition + direction;
        Rb.velocity = direction * moveSpeed;
    }

    Vector3 AdjustMoveForStairs(Vector3 direction)
    {
        endPosition = lastPosition + direction + direction;
        if (GameManager.instance.LookForWall(transform.position, direction, 2))
        {
            direction = (direction + transform.up).normalized;
            endPosition += transform.up;
        }
        else
            endPosition -= transform.up;
        stairs = true;
        return direction;
    }

    /// <summary>
    /// Checks if is at movement target and stop movements.
    /// </summary>
    void DuringMove()
    {
        Controller enemy = GameManager.instance.FindClosestPlayer(gameObject);
        if (enemy && enemy.moving && Vector3.Distance(enemy.endPosition, endPosition) <= 1 && Vector3.Distance(enemy.transform.position, transform.position) <= 1)
        {
            HitSelf(enemy, false);
            bool hit = CompareAction(enemy);
            if (hit)
                enemy.Hit(-enemy.transform.forward, gameObject, false);
            EndMove(endPosition - transform.forward);
        }
        
    }

    void ResetMove()
    {
        moving = false;
        recoil = false;
        stairs = false;
    }

    public void ResetPlayer()
    {
        lives = GameManager.instance.numberOfLives;
        transform.position = startPosition;
        gameObject.SetActive(false);
        GetComponentInChildren<MeshRenderer>().material = GameManager.instance.colorMats[playerIndex];
        hit = false;
        ResetMove();
        inputs.ResetButtons();
    }
    /// <summary>
    /// Looks if player is at position taken in argument.
    /// </summary>
    bool IsAtPosition(Vector3 position)
    {
        return Vector3.Distance(transform.position, position) <= minDist;
    }

    /// <summary>
    /// Looks if player is at position taken in argument.
    /// </summary>
    bool IsAtPosition(float start, float end)
    {
        return Mathf.Abs(start - end) <= minDist;
    }

    /// <summary>
    /// Code for after movement.
    /// </summary>
    /// <param name="endPosition"></param>
    void EndMove(Vector3 endPosition)
    {
        transform.position = new Vector3(endPosition.x, transform.position.y, endPosition.z);
        Rb.velocity = new Vector3(0, Rb.velocity.y, 0);
        if (grounded)
        {
            ResetMove();
            Rb.velocity = Vector3.zero;
            transform.position = new Vector3(endPosition.x, transform.position.y, endPosition.z);
        }

    }

    /// <summary>
    /// What happens when player is hit.
    /// </summary>
    void HitEffect()
    {
        if (hitTimer < hitTime)
            DuringHit();
        else
            EndHit();
    }

    /// <summary>
    /// During the hit.
    /// </summary>
    void DuringHit()
    {
        hitTimer += Time.deltaTime;
        model.GetComponent<MeshRenderer>().enabled = !model.GetComponent<MeshRenderer>().enabled; // For a flashing effect
    }

    /// <summary>
    /// After the hit.
    /// </summary>
    void EndHit()
    {
        hit = false;
        model.GetComponent<MeshRenderer>().enabled = true;
        hitTimer = 0;
    }

    /// <summary>
    /// Manages this player's inputs and actions.
    /// </summary>
    void InputManager()
    {
        inputs.SetDirection(ControllerName);
        if (lastDirection != inputs.direction)
            RotateToCurrentDirection();
        inputs.SetButtons(ControllerName);

        // Modify Action and Stance before move this frame for quicker stance change.
        SetAction();
        SetState(); // For now only material changes.

        if (inputs.MoveButton)
            moving = CanMove(transform.forward);
        inputs.ResetButtons();
    }

    /// <summary>
    /// Looks if this player can move when move input received.
    /// </summary>
    /// <returns> Is this player moving? </returns>
    bool CanMove(Vector3 direction)
    {
        bool _moving = false;
        if (LookForPlayerNoStairs(direction) || LookForPlayerDownWithStairs(direction))
            HitManager(_moving, direction, true);
        else if (LookForPlayerUpWithStairs(direction))
            HitManager(_moving, direction, false);
        else if (GameManager.instance.LookForWall(transform.position, direction, 1))
            _moving = false;
        else
            _moving = true;
        return _moving;
    }

    bool LookForPlayerNoStairs(Vector3 direction)
    {
        return (PlayerSeen(direction, 1)
            || PlayerSeen(direction - transform.up, 2))
            && !GameManager.instance.LookForStairs(transform.position, direction, 1);
    }

    bool LookForPlayerUpWithStairs(Vector3 direction)
    {
        return (PlayerSeen(direction + direction + transform.up, 3) && !PlayerSeen(direction, 1))
            && GameManager.instance.LookForStairs(transform.position, direction, 1);
    }

    bool LookForPlayerDownWithStairs(Vector3 direction)
    {
        return PlayerSeen(direction + direction - transform.up, 3)
            && GameManager.instance.LookForStairs(transform.position, direction, 1);
    }


    /// <summary>
    /// Looks for a player in front of this player and stores its in the otherPlayer variable
    /// </summary>
    /// <returns> Is a player in front of this player? </returns>
    bool PlayerSeen(Vector3 direction, float distance)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, distance, playerMask))
        {
            if (hit.collider.GetComponentInParent<Controller>())
            {
                otherPlayer = hit.collider.transform.parent.gameObject;
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Check if this player hits other player and vice-versa.
    /// </summary>
    /// <param name="_moving"> Temporary moving state </param>
    void HitManager(bool _moving, Vector3 direction, bool withRecoil)
    {
        _moving = CompareAction(EnemySeen);
        if (_moving)
            EnemySeen.Hit(direction, gameObject, withRecoil);
        else
            HitSelf(EnemySeen, withRecoil);
    }

    /// <summary>
    /// When this player is hit by other players
    /// </summary>
    /// <param name="direction"> Direction to where the player is getting bumped </param>
    void Hit(Vector3 direction, GameObject sender, bool withRecoil)
    {
        if (!hit)
            HitConfirm();
        if (lives <= 0)
            Dead(sender);
        else if (withRecoil)
            RecoilAfterHit(direction);

    }

    void HitConfirm()
    {
        lives--;
        hit = true;
        CameraManager.instance.ActivateEffect("ShakeHit");
    }

    /// <summary>
    /// This player is dead, his killer is the argument.
    /// </summary>
    void Dead(GameObject sender)
    {
        gameObject.SetActive(false);
        GameManager.instance.DeathOfPlayer(sender);
    }

    /// <summary>
    /// This player gets pushed after hit.
    /// </summary>
    /// <param name="direction"></param>
    void RecoilAfterHit(Vector3 direction)
    {
        recoil = CanMove(direction);
        if (recoil)
            StartMove(direction);
        if (GameManager.instance.LookForStairs(transform.position, direction - transform.up, 2)
            && recoil && !moving && !hit)
            endPosition += direction - transform.up;
    }

    /// <summary>
    /// Is player being countered by enemy.
    /// </summary>
    /// <param name="enemy"></param>
    bool HitSelf(Controller enemy, bool withRecoil)
    {
        bool _hit = enemy.CompareAction(this);
        if (_hit)
        {
            //That one context where you hit yourself at the bottom of the stairs.
            if (GameManager.instance.LookForStairs(transform.position, transform.forward, 1))
                withRecoil = true;
            if (moving)
                transform.position = endPosition;
            Hit(-transform.forward, otherPlayer, withRecoil);
        }
        return _hit;
    }

    /// <summary>
    /// Rotates this player toward current pointing direction.
    /// </summary>
    void RotateToCurrentDirection()
    {
        float newAngle = GetAngle(lastDirection, inputs.direction);
        transform.Rotate(new Vector3(0, newAngle, 0));
        lastDirection = inputs.direction;
    }

    /// <summary>
    /// Gets rotation angle based on Direction enum.
    /// </summary>
    /// <param name="lastDir">From direction</param>
    /// <param name="newDir">To direction</param>
    /// <returns> Euler angle for this rotation </returns>
    float GetAngle(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = GetAngleDelta(lastDir, newDir);
        if (Mathf.Abs(diff) == 2)
            return 180;
        else if (diff == 1)
            return 90;
        else if (diff == -1)
            return -90;
        return 0;
    }

    /// <summary>
    /// Gives an contant delta between two directions.
    /// </summary>
    /// <param name="lastDir"></param>
    /// <param name="newDir"></param>
    /// <returns> Integer delta for this angle </returns>
    int GetAngleDelta(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = 0;
        if (newDir == Inputs.Direction.Left && lastDir == Inputs.Direction.Up)
            diff = -1;
        else if (lastDir == Inputs.Direction.Left && newDir == Inputs.Direction.Up)
            diff = 1;
        else
            diff = (int)newDir - (int)lastDir;
        return diff;
    }

    /// <summary>
    /// Change this player's action according to its button inputs.
    /// </summary>
    void SetAction()
    {
        if (inputs.Button1)
            currentAction = Action.Jan;
        else if (inputs.Button2)
            currentAction = Action.Ken;
        else if (inputs.Button3)
            currentAction = Action.Po;
    }

    /// <summary>
    /// Change this player's state or stance according to its current action.
    /// </summary>
    void SetState()
    {
        switch (currentAction)
        {
            case Action.Jan:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[1];
                break;
            case Action.Ken:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[2];
                break;
            case Action.Po:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[0];
                break;
        }
    }

    /// <summary>
    /// Compare this player's action with other player in front of him. 
    /// </summary>
    /// <returns> True is this player wins VS other player </returns>
    public bool CompareAction(Controller enemy)
    {
        Action enemyAction = enemy.currentAction;
        switch (currentAction)
        {
            case Action.Jan:
                return enemyAction == Action.Ken;
            case Action.Ken:
                return enemyAction == Action.Po;
            case Action.Po:
                return enemyAction == Action.Jan;
        }
        return false;
    }

}
