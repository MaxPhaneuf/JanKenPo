﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Script for everything related to a player's controls in game world.
/// </summary>
public class Controller : MonoBehaviour
{
    #region Constants
    public const string HORIZONTAL_INPUT = "Horizontal";
    public const string VERTICAL_INPUT = "Vertical";
    public const string BUTTON1_INPUT = "Fire1";
    public const string BUTTON2_INPUT = "Fire2";
    public const string BUTTON3_INPUT = "Fire3";
    public const string MOVE_INPUT = "Jump";
    public const string START_INPUT = "Start";
    public const string NOT_PLAYING = "Not Playing";
    public enum Action { Jan, Ken, Po }
    #endregion

    #region Parameters
    // To be assign by GameManager instance
    [HideInInspector] public string ControllerName;
    Rigidbody Rb { get { return GetComponent<Rigidbody>(); } }
    Controller EnemySeen { get { return otherPlayer.GetComponent<Controller>(); } }
    #endregion

    #region Private attributes
    [HideInInspector] public bool moving, hit;
    Vector3 lastPosition;
    public Vector3 endPosition;
    float minDist = .1f;
    Inputs.Direction lastDirection = Inputs.Direction.Up;
    int lives;
    #endregion

    [Tooltip("This player's current inputs")]
    public Inputs inputs = new Inputs();
    [Tooltip("Layer for players")]
    public LayerMask playerMask;
    [Tooltip("Layer for obstacles and walls")]
    public LayerMask wallMask;
    [Tooltip("This player's current action")]
    public Action currentAction;
    [Tooltip("Other player seen by this player")]
    public GameObject otherPlayer;
    [Tooltip("This player's move speed (transform.forward * moveSpeed)")]
    public float moveSpeed = 7;
    [Tooltip("Total lives (Zero means defeat")]
    public int numberOfLives = 3;
    [HideInInspector] public bool keyboard;
    [HideInInspector] public int playerIndex;

    /// <summary>
    /// Internal Class to manage and show in inspector this player's inputs.
    /// </summary>
    [System.Serializable]
    public class Inputs
    {
        public float Horizontal, Vertical;
        public bool Button1, Button2, Button3, MoveButton;
        public enum Direction { Up, Right, Down, Left }
        [HideInInspector] public Direction direction;

        public void SetDirection(string ControllerName)
        {
            Horizontal = Input.GetAxisRaw(HORIZONTAL_INPUT + ControllerName);
            Vertical = Input.GetAxisRaw(VERTICAL_INPUT + ControllerName);
            if (Horizontal > 0)
                direction = Direction.Right;
            else if (Horizontal < 0)
                direction = Direction.Left;
            else if (Vertical > 0)
                direction = Direction.Up;
            else if (Vertical < 0)
                direction = Direction.Down;
        }

        public void SetButtons(string ControllerName)
        {
            Button1 = Input.GetButtonDown(BUTTON1_INPUT + ControllerName);
            Button2 = Input.GetButtonDown(BUTTON2_INPUT + ControllerName);
            Button3 = Input.GetButtonDown(BUTTON3_INPUT + ControllerName);
            MoveButton = Input.GetButtonDown(MOVE_INPUT + ControllerName);
        }

        public void ResetButtons()
        {
            Button1 = false;
            Button2 = false;
            Button3 = false;
            MoveButton = false;
        }
    }

    private void Start()
    {
        lives = numberOfLives;
    }

    void Update()
    {
        if (ControllerName != NOT_PLAYING)
        {
            if (!moving && !hit)
                BeforeMove();
            else
                DuringMove();
        }
    }

    /// <summary>
    /// Functions before and starting movement.
    /// </summary>
    void BeforeMove()
    {
        InputManager();
        if (moving)
            StartMove(transform.forward);
    }

    /// <summary>
    /// Function to start movement.
    /// </summary>
    void StartMove(Vector3 direction)
    {
        lastPosition = transform.position;
        endPosition = lastPosition + direction;
        Rb.velocity = direction * moveSpeed;
    }

    /// <summary>
    /// Checks if is at movement target and stop movements.
    /// </summary>
    void DuringMove()
    {
        if (PlayerSeen(transform.forward) && EnemySeen.moving)
            HitSelf(EnemySeen);
        if (IsAtPosition(endPosition))
            EndMove(endPosition);
    }

    /// <summary>
    /// Manages this player's inputs and actions.
    /// </summary>
    void InputManager()
    {
        inputs.SetDirection(ControllerName);
        if (lastDirection != inputs.direction)
            RotateToCurrentDirection();
        inputs.SetButtons(ControllerName);

        // Modify Action and Stance before move this frame for quicker stance change.
        SetAction();
        SetState(); // For now only material changes.

        if (inputs.MoveButton)
            moving = CanMove(transform.forward);
        inputs.ResetButtons();
    }

    /// <summary>
    /// Looks for a player in front of this player and stores its in the otherPlayer variable
    /// </summary>
    /// <returns> Is a player in front of this player? </returns>
    bool PlayerSeen(Vector3 direction)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit, 1, playerMask))
        {
            if (hit.collider.GetComponentInParent<Controller>())
            {
                otherPlayer = hit.collider.transform.parent.gameObject;
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Looks for a wall in front of this player.
    /// </summary>
    /// <returns> Is a wall in front of this player? </returns>
    bool WallSeen(Vector3 direction)
    {
        return Physics.Raycast(transform.position, direction, 1, wallMask);
    }

    /// <summary>
    /// Looks if this player can move when move input received.
    /// </summary>
    /// <returns> Is this player moving? </returns>
    bool CanMove(Vector3 direction)
    {
        bool _moving = false;
        if (PlayerSeen(direction))
            HitManager(_moving);
        else if (WallSeen(direction))
            _moving = false;
        else
            _moving = true;
        return _moving;
    }

    /// <summary>
    /// When this player is hit by other players
    /// </summary>
    /// <param name="direction"> Direction to where the player is getting bumped </param>
    void Hit(Vector3 direction, GameObject sender)
    {
        if (!hit)
            lives--;
        if (lives <= 0)
            Dead(sender);
        else
            RecoilAfterHit(direction);
    }

    /// <summary>
    /// This player is dead, his killer is the argument.
    /// </summary>
    void Dead(GameObject sender)
    {
        GameManager.instance.activePlayer--;
        if (GameManager.instance.activePlayer == 1)
            GameManager.instance.winner = sender;
        gameObject.SetActive(false);
    }

    /// <summary>
    /// This player gets pushed after hit.
    /// </summary>
    /// <param name="direction"></param>
    void RecoilAfterHit(Vector3 direction)
    {
        hit = CanMove(direction);
        if (hit)
            StartMove(direction);
    }

    /// <summary>
    /// Check if this player hits other player and vice-versa.
    /// </summary>
    /// <param name="_moving"> Temporary moving state </param>
    void HitManager(bool _moving)
    {
        Controller enemy = EnemySeen;
        _moving = CompareAction(enemy);
        if (_moving)
            enemy.Hit(transform.forward, gameObject);
        else
            HitSelf(enemy);
    }

    /// <summary>
    /// Is player being countered by enemy.
    /// </summary>
    /// <param name="enemy"></param>
    void HitSelf(Controller enemy)
    {
        bool _hit = enemy.CompareAction(this);
        if (_hit)
            Hit(-transform.forward, otherPlayer);
    }

    /// <summary>
    /// Looks if player is at position taken in argument.
    /// </summary>
    bool IsAtPosition(Vector3 position)
    {
        return Vector3.Distance(transform.position, position) <= minDist;
    }

    /// <summary>
    /// Code for after movement.
    /// </summary>
    /// <param name="endPosition"></param>
    void EndMove(Vector3 endPosition)
    {
        transform.position = endPosition;
        Rb.velocity = Vector3.zero;
        moving = false;
        if (hit)
            hit = false;
    }

    /// <summary>
    /// Rotates this player toward current pointing direction.
    /// </summary>
    void RotateToCurrentDirection()
    {
        float newAngle = GetAngle(lastDirection, inputs.direction);
        transform.Rotate(new Vector3(0, newAngle, 0));
        lastDirection = inputs.direction;
    }

    /// <summary>
    /// Gets rotation angle based on Direction enum.
    /// </summary>
    /// <param name="lastDir">From direction</param>
    /// <param name="newDir">To direction</param>
    /// <returns> Euler angle for this rotation </returns>
    float GetAngle(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = GetAngleDelta(lastDir, newDir);
        if (Mathf.Abs(diff) == 2)
            return 180;
        else if (diff == 1)
            return 90;
        else if (diff == -1)
            return -90;
        return 0;
    }

    /// <summary>
    /// Gives an contant delta between two directions.
    /// </summary>
    /// <param name="lastDir"></param>
    /// <param name="newDir"></param>
    /// <returns> Integer delta for this angle </returns>
    int GetAngleDelta(Inputs.Direction lastDir, Inputs.Direction newDir)
    {
        int diff = 0;
        if (newDir == Inputs.Direction.Left && lastDir == Inputs.Direction.Up)
            diff = -1;
        else if (lastDir == Inputs.Direction.Left && newDir == Inputs.Direction.Up)
            diff = 1;
        else
            diff = (int)newDir - (int)lastDir;
        return diff;
    }

    /// <summary>
    /// Change this player's action according to its button inputs.
    /// </summary>
    void SetAction()
    {
        if (inputs.Button1)
            currentAction = Action.Jan;
        else if (inputs.Button2)
            currentAction = Action.Ken;
        else if (inputs.Button3)
            currentAction = Action.Po;
    }

    /// <summary>
    /// Change this player's state or stance according to its current action.
    /// </summary>
    void SetState()
    {
        switch (currentAction)
        {
            case Action.Jan:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[0];
                break;
            case Action.Ken:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[1];
                break;
            case Action.Po:
                GetComponentInChildren<Renderer>().material = GameManager.instance.colorMats[2];
                break;
        }
    }

    /// <summary>
    /// Compare this player's action with other player in front of him. 
    /// </summary>
    /// <returns> True is this player wins VS other player </returns>
    public bool CompareAction(Controller enemy)
    {
        Action enemyAction = enemy.currentAction;
        switch (currentAction)
        {
            case Action.Jan:
                return enemyAction == Action.Ken;
            case Action.Ken:
                return enemyAction == Action.Po;
            case Action.Po:
                return enemyAction == Action.Jan;
        }
        return false;
    }

}
