﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    #region Private attributes
    int index;
    Quaternion endRotation;
    Vector3 endPosition;
    Queue<int> queue = new Queue<int>();
    #endregion
    
    /// <summary>
    /// Internal class to manage camera effects
    /// </summary>
    [System.Serializable]
    public class CameraEffect
    {

        [Tooltip("With a rotation or a movement effect?")]
        public bool withRotation, withMove;
        public RotationEffect rotationEffect;
        public MoveEffect moveEffect;
        [HideInInspector] public bool atTargetRot, atTargetPos;        

        [System.Serializable]
        public class RotationEffect
        {
            public enum Axis { X, Y, Z }
            [Tooltip("Axis to rotate Camera on")]
            public Axis axis;
            [Tooltip("Main camera rotation angle")]
            public float Angle;
            [Tooltip("Speed for this effect")]
            public float Speed;
        }
        
        [System.Serializable]
        public class MoveEffect
        {
            [Tooltip("Axis that will go to the target")]
            public bool x, y, z;
            [Tooltip("Move target for this effect")]
            public Vector3 Target;
            [Tooltip("Speed for this effect")]
            public float Speed;
        }
        
    }

    public List<CameraEffect> effects;
    [HideInInspector] public bool inRotEffect, inMoveEffect;
        
    public static CameraManager instance;
    
    void Start () {
        if (!instance)
            instance = this;
	}

    private void Update()
    {
        if (inRotEffect || inMoveEffect)
        {
            PlayCamEffect(index);
            if (effects[index].atTargetPos && effects[index].atTargetRot)
                PlayNextEffect();
        }
    }

    /// <summary>
    /// Play effect i in effect list.
    /// </summary>
    /// <param name="i"></param>
    public void PlayCamEffect(int i)
    {
        if(effects[i].withRotation)
            RotationEffect(effects[i].rotationEffect.Speed);
        if(effects[i].withMove)
            MoveEffect(effects[i].moveEffect.Speed);
    }

    /// <summary>
    /// Play the next effect in the queue.
    /// </summary>
    void PlayNextEffect()
    {
        if (queue.Count > 0)
        {
            index = queue.Dequeue();
            ActivateEffect(index);
        }
    }
    
    /// <summary>
    /// Activate effect i from list.
    /// </summary>
    /// <param name="i"></param>
    public void ActivateEffect(int i)
    {
        if (inMoveEffect || inRotEffect)
            queue.Enqueue(i);
        else
        {
            if (effects[i].withRotation)
            {
                endRotation = GetRotation(effects[i].rotationEffect.Angle, Camera.main.transform.rotation, effects[i].rotationEffect.axis);
                inRotEffect = true;
            }
            if (effects[i].withMove)
            {
                endPosition = GetPosition(effects[i].moveEffect.x, effects[i].moveEffect.y, effects[i].moveEffect.z, effects[i].moveEffect.Target, Camera.main.transform.position);
                inMoveEffect = true;
            }
            index = i;
        }
    }

    /// <summary>
    /// Rotation of Camera.main.
    /// </summary>
    /// <param name="speed"></param>
    void RotationEffect(float speed)
    {
        if (Vector3.Distance(Camera.main.transform.rotation.eulerAngles, endRotation.eulerAngles) > 0)
            Camera.main.transform.rotation = Quaternion.RotateTowards(Camera.main.transform.rotation, endRotation, speed);
        else
        {
            inRotEffect = false;
            effects[index].atTargetRot = true;
        }
    }

    /// <summary>
    /// Movement of Camera.main.
    /// </summary>
    /// <param name="speed"></param>
    void MoveEffect(float speed)
    {
        if (Vector3.Distance(Camera.main.transform.position, endPosition) > 0)
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, endPosition, speed);
        else
        {
            inMoveEffect = false;
            effects[index].atTargetPos = true;            
        }
        
    }
        
    /// <summary>
    /// Get new rotation with axis value (1 = X, 2 = Y, 3 = Z)
    /// </summary>
    Quaternion GetRotation(float angle, Quaternion rot, CameraEffect.RotationEffect.Axis axis)
    {
        switch(axis){
            case CameraEffect.RotationEffect.Axis.X:
                return Quaternion.Euler(rot.eulerAngles.x + angle, rot.eulerAngles.y, rot.eulerAngles.z);
            case CameraEffect.RotationEffect.Axis.Y:
                return Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y + angle, rot.eulerAngles.z);
            case CameraEffect.RotationEffect.Axis.Z:
                return Quaternion.Euler(rot.eulerAngles.x, rot.eulerAngles.y, rot.eulerAngles.z + angle);
            default:
                return rot;
        }
    }

    /// <summary>
    /// Position depending on chosen axis and target.
    /// </summary>
    Vector3 GetPosition(bool x, bool y, bool z, Vector3 target, Vector3 pos)
    {
        float _x = pos.x, _y = pos.y, _z = pos.z;
        if (x)
            _x = target.x;
        if (y)
            _y = target.y;
        if (z)
            _z = target.z;
        return new Vector3(_x, _y, _z);
    }

}
